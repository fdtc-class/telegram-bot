package ru.fdtc.bot.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.CommandUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;

@Slf4j
@Component
public class FdtcClassBot extends TelegramLongPollingBot {

    private final String botName;
    private final UpdateEventPublisher eventPublisher;

    public FdtcClassBot(BotConfig config, UpdateEventPublisher updateEventPublisher) throws TelegramApiException {
        super(config.getToken());
        botName = config.getName();
        this.eventPublisher = updateEventPublisher;
        execute(CommandUtil.getCommandsToSend());
    }

    @Override
    public void onUpdateReceived(Update update) {
        eventPublisher.publish(update, EventSource.EXTERNAL);
    }

    @Override
    public String getBotUsername() {
        return botName;
    }
}
