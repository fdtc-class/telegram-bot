package ru.fdtc.bot.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.exception.FdtcBotException;
import ru.fdtc.bot.common.util.ClassUtil;
import ru.fdtc.bot.handler.command.Command;
import ru.fdtc.bot.handler.command.CommandHandler;
import ru.fdtc.bot.handler.command.CommandStrategy;
import ru.fdtc.bot.handler.update.HandlerStrategy;
import ru.fdtc.bot.handler.update.UpdateHandler;

import java.util.EnumMap;
import java.util.List;

@Configuration
public class StrategyConfig {

    @Bean
    public HandlerStrategy handlerStrategy(List<UpdateHandler> handlers) {
        EnumMap<BotState, UpdateHandler> handlerMap = new EnumMap<>(BotState.class);
        handlers.forEach(handler -> {
            var state = handler.botState();
            if(handlerMap.containsKey(state)) {
                throw new FdtcBotException(state + "already has the handler '" +
                        ClassUtil.getClassName(handlerMap.get(state)) + ", unable to link " +
                        ClassUtil.getClassName(handler));
            }

            handlerMap.put(handler.botState(), handler);
        });

        return new HandlerStrategy(handlerMap);
    }

    @Bean
    public CommandStrategy commandStrategy(List<CommandHandler> handlers) {
        EnumMap<Command, CommandHandler> handlerMap = new EnumMap<>(Command.class);
        handlers.forEach(handler -> {
            var command = handler.command();
            if(handlerMap.containsKey(command)) {
                throw new FdtcBotException(command + "already has the handler '" +
                        ClassUtil.getClassName(handlerMap.get(command)) + ", unable to link " +
                        ClassUtil.getClassName(handler));
            }

            handlerMap.put(handler.command(), handler);
        });

        return new CommandStrategy(handlerMap);
    }
}
