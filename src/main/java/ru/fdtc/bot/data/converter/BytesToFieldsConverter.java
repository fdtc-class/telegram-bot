package ru.fdtc.bot.data.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.stereotype.Component;
import ru.fdtc.bot.data.entity.ClassCache;

import java.util.Map;
import java.util.UUID;

@Component
@ReadingConverter
public class BytesToFieldsConverter implements Converter<byte[], Map<UUID, ClassCache>> {
    private final GenericJackson2JsonRedisSerializer serializer;

    public BytesToFieldsConverter() {
        serializer = new GenericJackson2JsonRedisSerializer();
    }

    @Override
    public Map<UUID, ClassCache> convert(byte[] value) {
        return (Map<UUID, ClassCache>) serializer.deserialize(value);
    }
}
