package ru.fdtc.bot.data.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.convert.WritingConverter;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Component;
import ru.fdtc.bot.data.entity.ClassCache;

import java.util.Map;
import java.util.UUID;

@Component
@WritingConverter
public class FieldsToBytesConverter implements Converter<Map<UUID, ClassCache>, byte[]> {
    private final RedisSerializer serializer;

    public FieldsToBytesConverter() {
        serializer = new GenericJackson2JsonRedisSerializer();
    }

    @Override
    public byte[] convert(Map<UUID, ClassCache> value) {
        return serializer.serialize(value);
    }
}
