package ru.fdtc.bot.data.repository;

import org.springframework.data.repository.CrudRepository;
import ru.fdtc.bot.data.entity.UserConnectionInfo;

public interface UserConnectionInfoRepository extends CrudRepository<UserConnectionInfo, Long> {
}
