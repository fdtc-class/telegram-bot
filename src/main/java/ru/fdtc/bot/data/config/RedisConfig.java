package ru.fdtc.bot.data.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.convert.RedisCustomConversions;
import ru.fdtc.bot.data.converter.BytesToFieldsConverter;
import ru.fdtc.bot.data.converter.FieldsToBytesConverter;

import java.util.Arrays;

@Configuration
public class RedisConfig {

    @Bean
    public RedisCustomConversions redisCustomConversions(FieldsToBytesConverter longToBytes,
                                                         BytesToFieldsConverter bytesToLong) {
        return new RedisCustomConversions(Arrays.asList(longToBytes, bytesToLong));
    }
}
