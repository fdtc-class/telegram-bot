package ru.fdtc.bot.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class HungTaskFormCache {

    private String taskPerformanceId;
    private Boolean approve;
    private Integer points;
    private String cause;
}
