package ru.fdtc.bot.data.entity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.common.enumeration.UserRole;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TeacherFormCache {

    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotBlank
    private String username;
    private String password;
    @NotNull
    private UserRole role;
}
