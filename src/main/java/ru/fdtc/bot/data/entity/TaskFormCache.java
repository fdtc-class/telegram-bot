package ru.fdtc.bot.data.entity;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@Getter
@Setter
@Builder
public class TaskFormCache {
    private String description;
    private String name;
    private String classId;
    private Integer number;
    private Integer maximumPoints;

    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate expiredAt;
}
