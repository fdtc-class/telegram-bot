package ru.fdtc.bot.data.entity;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.rest.dto.enumeration.PassFormat;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClassFormCache {

    private String description;
    @NotBlank
    private String name;
    @NotNull
    private PassFormat passFormat;
    @NotBlank
    private String subjectId;
}
