package ru.fdtc.bot.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.rest.dto.enumeration.PassFormat;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ClassCache implements Serializable {

    private String id;
    private String className;
    private String subjectName;
    private String description;
    private Integer requiredPoints;
    private Integer currentPoints;
    private PassFormat passFormat;
    private String teacherName;
}
