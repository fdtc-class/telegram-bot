package ru.fdtc.bot.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.redis.core.RedisHash;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.UserRole;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Data
@Builder
@RedisHash("UserConnectionInfo")
@AllArgsConstructor
public class UserConnectionInfo implements Serializable {

    private Long id;
    private BotState state;
    private String email;
    private String token;
    private UserRole role;
    private String firstname;
    private String lastname;
    private List<Integer> sentMessages;
    private String openClassId;
    private String openTaskId;
    private String openHangedTaskId;
    private TeacherFormCache teacherForm;
    private TaskFormCache taskForm;
    private HungTaskFormCache hungTaskFormCache;
    private ClassFormCache classForm;
    private SubjectFormCache subjectForm;

    public UserConnectionInfo() {
        sentMessages = new CopyOnWriteArrayList<>();
    }

    public void clearCachedInfo() {
        openClassId = null;
        openTaskId = null;
    }
}