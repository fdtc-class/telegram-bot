package ru.fdtc.bot.data.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.rest.dto.enumeration.TaskPerformanceStatus;
import ru.fdtc.bot.rest.dto.enumeration.TaskStatus;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TaskCache {

    private String id;
    private String name;
    private Integer number;
    private Integer maximumPints;
    private Integer minimumPoints;
    private Integer receivedPoints;
    private TaskStatus status;
    private String description;
    private String fileName;
    private TaskPerformanceStatus performanceStatus;
    private String submittedFileName;
    private String teacherFeedback;
}
