package ru.fdtc.bot.data.entity;

import jakarta.validation.constraints.NotBlank;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SubjectFormCache {

    @NotBlank
    private String name;
    private String description;
}
