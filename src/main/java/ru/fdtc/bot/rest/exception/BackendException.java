package ru.fdtc.bot.rest.exception;

public class BackendException extends RuntimeException {

    public BackendException(String message) {
        super(message);
    }
}
