package ru.fdtc.bot.rest.dto.request;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Getter
@Builder
public class GetTokenRq implements Serializable {

    private String username;
    private String password;

}
