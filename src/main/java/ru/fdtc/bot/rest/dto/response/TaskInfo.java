package ru.fdtc.bot.rest.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.rest.dto.enumeration.TaskStatus;
import ru.fdtc.bot.rest.dto.sealed.TaskPerformanceInfo;

@Getter
@NoArgsConstructor
public class TaskInfo {

    private String id;
    private String name;
    private Integer number;
    private Integer maximumPints;
    private Integer minimumPoints;
    private TaskStatus status;
    private String description;
    private String fileName;
    private TaskPerformanceInfo taskPerformanceInfo;
}
