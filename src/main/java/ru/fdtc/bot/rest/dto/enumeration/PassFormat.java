package ru.fdtc.bot.rest.dto.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum PassFormat {

  EXAM("Экзамен"), DIFF_TEST("Дифференцированный зачёт"), TEST("Зачёт");

  private final String name;
}
