package ru.fdtc.bot.rest.dto.enumeration;

import lombok.NoArgsConstructor;

public enum ClassStatus {
  ACTIVE, END
}
