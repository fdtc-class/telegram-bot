package ru.fdtc.bot.rest.dto.enumeration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TaskStatus {
    ACTIVE("Активно"),
    EXPIRED("Истекло"),
    HIDDEN("Скрыто");

    private final String value;
}
