package ru.fdtc.bot.rest.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class GetSubjectsRs {

    private String id;
    private String name;
}
