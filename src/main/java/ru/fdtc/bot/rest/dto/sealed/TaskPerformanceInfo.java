package ru.fdtc.bot.rest.dto.sealed;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.rest.dto.enumeration.TaskPerformanceStatus;

@Getter
@NoArgsConstructor
public class TaskPerformanceInfo {

    private String id;
    private TaskPerformanceStatus status;
    private String fileName;
    private String cause;
    private Integer points;
}