package ru.fdtc.bot.rest.dto.sealed;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class Professor {

    private String firstName;
    private String lastName;
}
