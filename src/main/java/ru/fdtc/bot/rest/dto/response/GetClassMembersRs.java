package ru.fdtc.bot.rest.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class GetClassMembersRs {

    private String id;
    private String firstName;
    private String lastName;
    private Integer currentPoints;
    private String avatar;
    private String groupName;
}
