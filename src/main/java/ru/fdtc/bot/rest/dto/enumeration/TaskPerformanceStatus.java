package ru.fdtc.bot.rest.dto.enumeration;

public enum TaskPerformanceStatus {

    WAITING_FOR_HANDING, ON_CHECK, CONFIRM, DECLINED
}
