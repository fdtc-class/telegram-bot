package ru.fdtc.bot.rest.dto.enumeration;

public enum EntityType {
    TASK, CLASS, HANDING_FOR_TASK
}
