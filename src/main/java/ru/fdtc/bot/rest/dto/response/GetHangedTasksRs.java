package ru.fdtc.bot.rest.dto.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor
public class GetHangedTasksRs {

    private String id;
    private String studentFirstName;
    private String studentLastName;
    private String studentAvatar;
    private String fileName;
    private Integer taskNumber;
    private Integer maximumPoints;
    private Integer points;

    @JsonDeserialize(using = LocalDateDeserializer.class)
    private LocalDate hangedAt;
    private Boolean expired;
    private String cause;
}
