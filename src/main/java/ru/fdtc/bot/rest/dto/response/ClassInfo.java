package ru.fdtc.bot.rest.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.rest.dto.enumeration.PassFormat;
import ru.fdtc.bot.rest.dto.sealed.Professor;

@Getter
@NoArgsConstructor
public class ClassInfo {

    private String id;
    private String className;
    private String description;
    private Integer requiredPoints;
    private Integer currentPoints;
    private Professor professor;
    private PassFormat passFormat;
    private String subjectName;
}
