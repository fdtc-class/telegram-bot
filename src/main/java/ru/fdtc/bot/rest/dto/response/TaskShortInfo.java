package ru.fdtc.bot.rest.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.fdtc.bot.rest.dto.enumeration.TaskStatus;
import ru.fdtc.bot.rest.dto.sealed.TaskPerformanceInfo;

@Getter
@NoArgsConstructor
public class TaskShortInfo {

    private String id;
    private String name;
    private Integer number;
    private TaskStatus status;

    @JsonProperty("taskPerformanceInfo")
    private TaskPerformanceInfo performance;
}
