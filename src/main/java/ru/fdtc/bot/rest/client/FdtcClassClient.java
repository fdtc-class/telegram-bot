package ru.fdtc.bot.rest.client;

import feign.Response;
import jakarta.validation.Valid;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.fdtc.bot.data.entity.*;
import ru.fdtc.bot.rest.FeignConfig;
import ru.fdtc.bot.rest.dto.MultipartFileImpl;
import ru.fdtc.bot.rest.dto.enumeration.ClassStatus;
import ru.fdtc.bot.rest.dto.enumeration.EntityType;
import ru.fdtc.bot.rest.dto.enumeration.TaskPerformanceStatus;
import ru.fdtc.bot.rest.dto.enumeration.TaskStatus;
import ru.fdtc.bot.rest.dto.request.GetTokenRq;
import ru.fdtc.bot.rest.dto.response.*;

import java.util.List;

@FeignClient(value = "fdtc-backend", url = "${rest.backend.url}", configuration = FeignConfig.class)
public interface FdtcClassClient {

    @PostMapping(value = "/api/auth")
    AuthenticationResponse authenticate(@RequestBody GetTokenRq request);

    @GetMapping("/api/class/all")
    List<ClassShortInfo> getUserClasses(@RequestHeader(HttpHeaders.AUTHORIZATION) String token);

    @GetMapping("/api/class/{classId}/members")
    List<GetClassMembersRs> getClassMembers(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                            @PathVariable String classId);

    @GetMapping("/api/class/all/{status}")
    List<ClassShortInfo> getProfessorClasses(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                             @PathVariable ClassStatus status);

    @PostMapping(value = "/api/class", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    String createClass(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                       @RequestPart("rq") ClassFormCache rq,
                       @RequestPart MultipartFileImpl image);

    @GetMapping("/api/class/subjects")
    List<GetSubjectsRs> getSubjects(@RequestHeader(HttpHeaders.AUTHORIZATION) String token);

    @PostMapping("/api/class/subject")
    void addSubject(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                    @RequestBody SubjectFormCache rq);

    @GetMapping("/api/class/groups")
    List<GetGroupsRs> getGroups(@RequestHeader(HttpHeaders.AUTHORIZATION) String token);

    @PostMapping("/api/class/addGroup")
    void addGroupMembersToClass(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                @RequestParam("groupId") String groupId,
                                @RequestParam("classId") String classId);

    @GetMapping("/api/class/{id}")
    ClassInfo getClassById(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                           @PathVariable String id);

    @GetMapping("/api/class/{classId}/resources")
    List<String> getClassResources(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                   @PathVariable String classId);

    @GetMapping("/api/class/{classId}/tasks")
    List<TaskShortInfo> getClassTasks(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                      @PathVariable String classId);

    @GetMapping("/api/class/{classId}/tasks/{taskId}")
    TaskInfo getClassTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                          @PathVariable String classId, @PathVariable String taskId);

    @PostMapping("/api/class/task")
    String addTaskToClass(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                          @RequestBody TaskFormCache rq);

    @GetMapping("/api/class/{classId}/taskNumbers")
    List<Integer> getClassTaskNumbers(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                      @PathVariable String classId);

    @PostMapping(value = "/api/resource/hangTask", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    void hangTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                  @RequestPart(value = "file") MultipartFileImpl file,
                  @RequestParam("taskId") String taskId);

    @GetMapping("/api/class/hangedTasks")
    List<GetHangedTasksRs> getHangedTasks(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                          @RequestParam("classId") String classId,
                                          @RequestParam("status") TaskPerformanceStatus status);

    @GetMapping("/api/class/hangedTasks/{taskPerformanceId}")
    GetHangedTasksRs getHangedTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                                   @RequestParam("classId") String classId,
                                   @RequestParam("status") TaskPerformanceStatus status,
                                   @PathVariable String taskPerformanceId);

    @PostMapping("/api/class/review")
    void reviewTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                    @RequestBody HungTaskFormCache rq);

    @PostMapping("/api/class/{taskId}/activate")
    TaskStatus activateTask(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                            @PathVariable String taskId);

    @GetMapping("/api/resource")
    Response getResource(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                         @RequestParam("entityId") String entityId,
                         @RequestParam("entityType") EntityType entityType,
                         @RequestParam("fileName") String fileName);

    @PostMapping(value = "/api/resource", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    void addResource(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                     @RequestPart MultipartFile file,
                     @RequestParam("entityId") String entityId,
                     @RequestParam("entityType") EntityType entityType);

    @PostMapping("/api/profile/professor")
    String createProfessor(@RequestHeader(HttpHeaders.AUTHORIZATION) String token,
                           @RequestBody @Valid TeacherFormCache rq);
}
