package ru.fdtc.bot.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.fdtc.bot.common.dto.LoginResponse;
import ru.fdtc.bot.common.enumeration.UserRole;
import ru.fdtc.bot.common.exception.LoginException;
import ru.fdtc.bot.rest.client.FdtcClassClient;
import ru.fdtc.bot.rest.dto.request.GetTokenRq;
import ru.fdtc.bot.rest.dto.response.AuthenticationResponse;

@Slf4j
@Service
@RequiredArgsConstructor
public class AuthorizationService {

    private final FdtcClassClient client;

    public LoginResponse login(String email, String password) {
        try {
            return doLogin(email, password);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new LoginException(e.getMessage());
        }
    }

    private LoginResponse doLogin(String email, String password) {
        GetTokenRq request = GetTokenRq.builder()
                .username(email)
                .password(password)
                .build();
        AuthenticationResponse authenticationResponse = client.authenticate(request);
        return LoginResponse.builder()
                .role(UserRole.of(authenticationResponse.getRole()))
                .token(authenticationResponse.getToken())
                .build();
    }
}
