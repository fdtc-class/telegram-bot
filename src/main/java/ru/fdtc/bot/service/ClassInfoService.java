package ru.fdtc.bot.service;

import feign.Response;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import ru.fdtc.bot.common.exception.ChatNotFoundException;
import ru.fdtc.bot.common.exception.FileDownloadingException;
import ru.fdtc.bot.common.mapper.ClassMapper;
import ru.fdtc.bot.common.util.TokenUtil;
import ru.fdtc.bot.data.entity.ClassCache;
import ru.fdtc.bot.data.entity.UserConnectionInfo;
import ru.fdtc.bot.data.repository.UserConnectionInfoRepository;
import ru.fdtc.bot.rest.client.FdtcClassClient;
import ru.fdtc.bot.rest.dto.MultipartFileImpl;
import ru.fdtc.bot.rest.dto.enumeration.ClassStatus;
import ru.fdtc.bot.rest.dto.enumeration.EntityType;
import ru.fdtc.bot.rest.dto.response.ClassShortInfo;
import ru.fdtc.bot.rest.dto.response.GetClassMembersRs;
import ru.fdtc.bot.rest.dto.response.GetGroupsRs;
import ru.fdtc.bot.rest.dto.response.GetSubjectsRs;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ClassInfoService {

    private final FdtcClassClient client;
    private final ClassMapper classMapper;
    private final UserConnectionInfoRepository repository;

    public List<ClassShortInfo> findActiveClasses(Long chatId) {
        var user = getUser(chatId);

        return switch (user.getRole()) {
            case STUDENT -> client.getUserClasses(TokenUtil.prepare(user.getToken()));
            case ADMIN, TEACHER -> client.getProfessorClasses(TokenUtil.prepare(user.getToken()), ClassStatus.ACTIVE);
        };
    }

    public List<GetGroupsRs> getGroups(Long chatId) {
        var user = getUser(chatId);
        return client.getGroups(TokenUtil.prepare(user.getToken()));
    }

    public void addGroupToClass(Long chatId, String groupId) {
        var user = getUser(chatId);
        client.addGroupMembersToClass(TokenUtil.prepare(user.getToken()), groupId, user.getOpenClassId());
    }

    public List<GetClassMembersRs> findClassPerformance(Long chatId) {
        var user = getUser(chatId);
        return client.getClassMembers(TokenUtil.prepare(user.getToken()), user.getOpenClassId());
    }

    public void create(Long chatId) {
        var user = getUser(chatId);
        var classForm = user.getClassForm();
        var classId = client.createClass(TokenUtil.prepare(user.getToken()), classForm, null);
        user.setOpenClassId(classId);
        repository.save(user);
    }

    public List<GetSubjectsRs> getSubjects(Long chatId) {
        var user = getUser(chatId);
        return client.getSubjects(TokenUtil.prepare(user.getToken()));
    }

    public void addSubject(Long chatId) {
        var user = getUser(chatId);
        client.addSubject(TokenUtil.prepare(user.getToken()), user.getSubjectForm());
    }

    public void save(Long chatId, String classId) {
        var user = getUser(chatId);
        user.setOpenClassId(classId);
        repository.save(user);
    }


    public ClassCache getCurrent(Long chatId) {
        var user = getUser(chatId);

        var clazz = client.getClassById(TokenUtil.prepare(user.getToken()), user.getOpenClassId());
        return classMapper.map(clazz);
    }

    public List<InputFile> getClassMaterials(Long chatId) {
        var user = getUser(chatId);

        var filenames = client.getClassResources(TokenUtil.prepare(user.getToken()), user.getOpenClassId());
        return filenames.stream()
                .map(filename -> {
                    var response = client.getResource(TokenUtil.prepare(user.getToken()), user.getOpenClassId(), EntityType.CLASS, filename);
                    validateResponse(response);
                    try {
                        var stream = response.body().asInputStream();
                        return new InputFile(stream, filename);
                    } catch (IOException e) {
                        throw new FileDownloadingException("Unable to download file", e);
                    }
                }).toList();
    }

    public void addClassMaterial(Long chatId, InputStream stream, String fileName, String contentType) {
        var user = getUser(chatId);
        var classId = user.getOpenClassId();
        var multipartFile = new MultipartFileImpl(fileName, contentType, stream);
        client.addResource(TokenUtil.prepare(user.getToken()), multipartFile, classId, EntityType.CLASS);
    }

    private void validateResponse(Response response) {
        if(response.status() != HttpStatus.OK.value()) {
            throw new FileDownloadingException("Unable to download, status is " + response.status());
        }
    }

    private UserConnectionInfo getUser(Long chatId) {
        return repository.findById(chatId)
                .orElseThrow(() -> new ChatNotFoundException("Chat '%s' is not found".formatted(chatId)));
    }
}
