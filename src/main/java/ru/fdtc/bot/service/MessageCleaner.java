package ru.fdtc.bot.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.bots.AbsSender;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.data.repository.UserConnectionInfoRepository;

import java.util.Collections;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class MessageCleaner {

    private final AbsSender sender;
    private final UserConnectionInfoRepository repository;

    public void clearSent(Long chatId) {
        var messages = sentMessages(chatId);
        messages.forEach(message -> {
            try {
                deleteMessage(chatId, message);
            } catch (TelegramApiException e) {
                log.warn("Unable to delete message.", e);
            }
        });
    }

    public void clearReceived(Long chatId, Integer messageId) {
        var messages = sentMessages(chatId);
        if(!messages.contains(messageId)) {
            try {
                deleteMessage(chatId, messageId);
            } catch (TelegramApiException e) {
                log.warn("Unable to delete message.", e);
            }
        }
    }

    private void deleteMessage(Long chatId, Integer messageId) throws TelegramApiException {
        var deleteMessage = DeleteMessage.builder()
                .chatId(chatId)
                .messageId(messageId)
                .build();
        sender.execute(deleteMessage);
    }

    private List<Integer> sentMessages(Long chatId) {
        var user = repository.findById(chatId);
        return user.isPresent() ? user.get().getSentMessages() : Collections.emptyList();
    }
}
