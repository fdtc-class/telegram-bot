package ru.fdtc.bot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.UserRole;
import ru.fdtc.bot.data.entity.UserConnectionInfo;
import ru.fdtc.bot.data.repository.UserConnectionInfoRepository;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class UserConnectionInfoService {

    private final UserConnectionInfoRepository repository;

    public UserConnectionInfo findById(Long chatId) {
        return repository.findById(chatId).orElseGet(() -> UserConnectionInfo.builder()
                .id(chatId)
                .state(BotState.START)
                .build());
    }

    public boolean isAuthorized(Long chatId) {
        if(!repository.existsById(chatId)) {
            return false;
        }

        return Objects.nonNull(findById(chatId).getToken());
    }

    public void refresh(Long chatId) {
        repository.deleteById(chatId);
        repository.save(findById(chatId));
    }

    public void save(UserConnectionInfo userConnectionInfo) {
        repository.save(userConnectionInfo);
    }

    public void clearForms(Long chatId) {
        var user = findById(chatId);
        user.setTaskForm(null);
        user.setClassForm(null);
        user.setSubjectForm(null);
        user.setTeacherForm(null);
    }

    public void setState(Long chatId, BotState state) {
        var user = findById(chatId);
        user.setState(state);
        repository.save(user);
    }

    public void setEmail(Long chatId, String email) {
        var user = findById(chatId);
        user.setEmail(email);
        repository.save(user);
    }

    public void setToken(Long chatId, String token) {
        var user = findById(chatId);
        user.setToken(token);
        repository.save(user);
    }

    public UserRole getRole(Long chatId) {
        return findById(chatId).getRole();
    }

    public void setRole(Long chatId, UserRole role) {
        var user = findById(chatId);
        user.setRole(role);
        repository.save(user);
    }
}
