package ru.fdtc.bot.service;

import feign.Response;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import ru.fdtc.bot.common.exception.ChatNotFoundException;
import ru.fdtc.bot.common.exception.FileDownloadingException;
import ru.fdtc.bot.common.mapper.TaskMapper;
import ru.fdtc.bot.common.util.TokenUtil;
import ru.fdtc.bot.data.entity.TaskCache;
import ru.fdtc.bot.data.entity.UserConnectionInfo;
import ru.fdtc.bot.data.repository.UserConnectionInfoRepository;
import ru.fdtc.bot.rest.client.FdtcClassClient;
import ru.fdtc.bot.rest.dto.MultipartFileImpl;
import ru.fdtc.bot.rest.dto.enumeration.EntityType;
import ru.fdtc.bot.rest.dto.enumeration.TaskPerformanceStatus;
import ru.fdtc.bot.rest.dto.response.GetHangedTasksRs;
import ru.fdtc.bot.rest.dto.response.TaskShortInfo;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskInfoService {

    private static final String CHAT_NOT_FOUND = "Chat '%s' is not found";

    private final FdtcClassClient client;
    private final UserConnectionInfoRepository repository;
    private final TaskMapper mapper;

    public List<TaskShortInfo> findTasks(Long chatId) {
        var user = getUser(chatId);
        return client.getClassTasks(TokenUtil.prepare(user.getToken()), user.getOpenClassId());
    }

    public List<GetHangedTasksRs> findHangedTasks(Long chatId) {
        var user = getUser(chatId);
        return client.getHangedTasks(TokenUtil.prepare(user.getToken()), user.getOpenClassId(), TaskPerformanceStatus.ON_CHECK);
    }

    public void save(Long chatId, String taskId) {
        var user = getUser(chatId);
        user.setOpenTaskId(taskId);
        repository.save(user);
    }

    public void saveHanged(Long chatId, String hangedTaskId) {
        var user = getUser(chatId);
        user.setOpenHangedTaskId(hangedTaskId);
        repository.save(user);
    }

    public boolean isNumberExist(Long chatId, Integer number) {
        var user = getUser(chatId);
        var numbers = client.getClassTaskNumbers(TokenUtil.prepare(user.getToken()), user.getOpenClassId());

        return numbers.contains(number);
    }

    public TaskCache getCurrent(Long chatId) {
        var user = repository.findById(chatId)
                .orElseThrow(() -> new ChatNotFoundException(CHAT_NOT_FOUND.formatted(chatId)));

        var response = client.getClassTask(TokenUtil.prepare(user.getToken()), user.getOpenClassId(), user.getOpenTaskId());
        return mapper.map(response);
    }

    public InputFile getMaterial(Long chatId) {
        var user = getUser(chatId);

        var task = client.getClassTask(TokenUtil.prepare(user.getToken()), user.getOpenClassId(), user.getOpenTaskId());

        if(StringUtils.isBlank(task.getFileName())) {
            return null;
        }

        var response = client.getResource(TokenUtil.prepare(user.getToken()), task.getId(), EntityType.TASK, task.getFileName());
        validateResponse(response);
        try {
            var stream = response.body().asInputStream();
            return new InputFile(stream, task.getFileName());
        } catch (IOException e) {
            throw new FileDownloadingException("Unable to download file", e);
        }
    }

    public void create(Long chatId) {
        var user = getUser(chatId);
        var taskForm = user.getTaskForm();
        var taskId = client.addTaskToClass(TokenUtil.prepare(user.getToken()), taskForm);
        user.setOpenTaskId(taskId);
        repository.save(user);
    }

    public void addTaskMaterial(Long chatId, InputStream stream, String fileName, String contentType) {
        var user = getUser(chatId);
        var taskId = user.getOpenTaskId();
        var multipartFile = new MultipartFileImpl(fileName, contentType, stream);
        client.addResource(TokenUtil.prepare(user.getToken()), multipartFile, taskId, EntityType.TASK);
    }

    public InputFile getHangedTaskFile(Long chatId) {
        var user = getUser(chatId);
        var hangedTaskId = user.getOpenHangedTaskId();
        var hangedTask = client.getHangedTask(TokenUtil.prepare(user.getToken()), user.getOpenClassId(), TaskPerformanceStatus.ON_CHECK, hangedTaskId);
        var response = client.getResource(TokenUtil.prepare(user.getToken()), hangedTaskId, EntityType.HANDING_FOR_TASK, hangedTask.getFileName());

        validateResponse(response);
        try {
            var stream = response.body().asInputStream();
            return new InputFile(stream, hangedTask.getFileName());
        } catch (IOException e) {
            throw new FileDownloadingException("Unable to download file", e);
        }
    }

    public void review(Long chatId) {
        var user = getUser(chatId);
        client.reviewTask(TokenUtil.prepare(user.getToken()), user.getHungTaskFormCache());
    }

    public void activate(Long chatId) {
        var user = getUser(chatId);
        client.activateTask(TokenUtil.prepare(user.getToken()), user.getOpenTaskId());
    }

    public void submit(Long chatId, InputStream stream, String fileName, String contentType) {
        var user = getUser(chatId);

        var task = client.getClassTask(TokenUtil.prepare(user.getToken()), user.getOpenClassId(), user.getOpenTaskId());
        var multipartFile = new MultipartFileImpl(fileName, contentType, stream);
        client.hangTask(TokenUtil.prepare(user.getToken()), multipartFile, task.getId());
    }

    private void validateResponse(Response response) {
        if(response.status() != HttpStatus.OK.value()) {
            throw new FileDownloadingException("Unable to download, status is " + response.status());
        }
    }

    private UserConnectionInfo getUser(Long chatId) {
        return repository.findById(chatId)
                .orElseThrow(() -> new ChatNotFoundException(CHAT_NOT_FOUND.formatted(chatId)));
    }
}
