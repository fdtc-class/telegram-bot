package ru.fdtc.bot.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.fdtc.bot.common.exception.ChatNotFoundException;
import ru.fdtc.bot.common.util.TokenUtil;
import ru.fdtc.bot.data.repository.UserConnectionInfoRepository;
import ru.fdtc.bot.rest.client.FdtcClassClient;

@Service
@RequiredArgsConstructor
public class AdminService {

    private static final String CHAT_NOT_FOUND = "Chat '%s' is not found";

    private final UserConnectionInfoRepository repository;
    private final FdtcClassClient client;

    public String addTeacher(Long chatId) {
        var user = repository.findById(chatId)
                .orElseThrow(() -> new ChatNotFoundException(CHAT_NOT_FOUND.formatted(chatId)));

        return client.createProfessor(TokenUtil.prepare(user.getToken()), user.getTeacherForm());
    }
}
