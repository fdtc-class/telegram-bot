package ru.fdtc.bot.event;

import lombok.Getter;
import lombok.ToString;
import org.springframework.context.ApplicationEvent;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.EventSource;

@Getter
@ToString
public class UpdateEvent extends ApplicationEvent {

    private final Update update;
    private final EventSource eventSource;

    public UpdateEvent(Object source, Update update, EventSource eventSource) {
        super(source);
        this.update = update;
        this.eventSource = eventSource;
    }
}
