package ru.fdtc.bot.event;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.EventSource;

@Slf4j
@Component
@RequiredArgsConstructor
public class UpdateEventPublisher {

    private final ApplicationEventPublisher publisher;

    public void publish(Update update, EventSource eventSource) {
        var event = new UpdateEvent(this, update, eventSource);
        log.debug("Publishing update event: {}", event);
        publisher.publishEvent(event);
    }
}
