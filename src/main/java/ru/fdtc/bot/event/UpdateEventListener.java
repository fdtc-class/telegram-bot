package ru.fdtc.bot.event;

import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import ru.fdtc.bot.common.util.CommandUtil;
import ru.fdtc.bot.handler.command.CommandManager;
import ru.fdtc.bot.handler.update.UpdateManager;

@Component
@RequiredArgsConstructor
public class UpdateEventListener implements ApplicationListener<UpdateEvent> {

    private final UpdateManager updateManager;
    private final CommandManager commandManager;

    @Override
    public void onApplicationEvent(UpdateEvent event) {
        var update = event.getUpdate();
        if(CommandUtil.isCommand(update)) {
            commandManager.manage(update);
            return;
        }

        updateManager.manage(event.getUpdate(), event.getEventSource());
    }
}
