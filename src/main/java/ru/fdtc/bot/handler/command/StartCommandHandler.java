package ru.fdtc.bot.handler.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.MessageCleaner;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class StartCommandHandler extends DefaultCommandHandler {

    private final UserConnectionInfoService connectionInfoService;
    private final UpdateEventPublisher publisher;

    public StartCommandHandler(MessageCleaner messageCleaner,
                               UserConnectionInfoService connectionInfoService,
                               UpdateEventPublisher publisher) {
        super(messageCleaner);
        this.connectionInfoService = connectionInfoService;
        this.publisher = publisher;
    }

    @Override
    public void doHandle(Update update) {
        var chatId = UpdateUtil.getChatId(update);
        if(connectionInfoService.isAuthorized(chatId)) {
            getMessageCleaner().clearReceived(chatId, update.getMessage().getMessageId());
            switch (connectionInfoService.getRole(chatId)) {
                case STUDENT -> connectionInfoService.setState(chatId, BotState.STUDENT_MAIN_PAGE);
                case TEACHER -> connectionInfoService.setState(chatId, BotState.TEACHER_MAIN_PAGE);
                case ADMIN -> connectionInfoService.setState(chatId, BotState.ADMIN_CHOOSE_ROLE);
            }
            publisher.publish(update, EventSource.INTERNAL);
        } else {
            connectionInfoService.setState(chatId, BotState.START);
            publisher.publish(update, EventSource.EXTERNAL);
        }
    }

    @Override
    public Command command() {
        return Command.START;
    }
}
