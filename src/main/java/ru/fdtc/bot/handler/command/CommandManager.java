package ru.fdtc.bot.handler.command;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.CommandUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.data.entity.UserConnectionInfo;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
@RequiredArgsConstructor
public class CommandManager {

    private final CommandStrategy strategy;
    private final UserConnectionInfoService userConnectionInfoService;
    private final UpdateEventPublisher publisher;

    public void manage(Update update) {

        var chatId = UpdateUtil.getChatId(update);
        var connectionInfo = userConnectionInfoService.findById(chatId);
        var command = CommandUtil.getCommand(update);
        var handler = strategy.getHandler(command);

        if(handler.isPresent()) {
            Try.run(() -> handler.get().handle(update))
                    .onFailure(t -> {
                        log.error("Unable to handle {}. Reason: {}", handler.get().command(), t.getMessage(), t);
                        handleError(connectionInfo, update);
                    });

        } else {
            log.warn("No handler found for such command: {}", command);
            handleError(connectionInfo, update);
        }
    }

    private void handleError(UserConnectionInfo connectionInfo, Update update) {
        connectionInfo.setState(BotState.ERROR);
        connectionInfo.clearCachedInfo();
        userConnectionInfoService.save(connectionInfo);
        publisher.publish(update, EventSource.INTERNAL);
    }
}
