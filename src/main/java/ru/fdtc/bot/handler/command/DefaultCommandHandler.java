package ru.fdtc.bot.handler.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.service.MessageCleaner;

@Getter
@RequiredArgsConstructor
public abstract class DefaultCommandHandler implements CommandHandler {

    private final MessageCleaner messageCleaner;

    @Override
    public void handle(Update update) {
        var chatId = UpdateUtil.getChatId(update);
        messageCleaner.clearSent(chatId);
        update.getMessage().setText(null);
        doHandle(update);
    }

    public abstract void doHandle(Update update);
}
