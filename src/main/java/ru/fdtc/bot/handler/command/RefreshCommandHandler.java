package ru.fdtc.bot.handler.command;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.MessageCleaner;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class RefreshCommandHandler extends DefaultCommandHandler {

    private final UserConnectionInfoService connectionInfoService;
    private final UpdateEventPublisher publisher;

    public RefreshCommandHandler(MessageCleaner messageCleaner,
                                 UserConnectionInfoService connectionInfoService,
                                 UpdateEventPublisher publisher) {
        super(messageCleaner);
        this.connectionInfoService = connectionInfoService;
        this.publisher = publisher;
    }

    @Override
    public void doHandle(Update update) {
        var chatId = UpdateUtil.getChatId(update);
        getMessageCleaner().clearReceived(chatId, update.getMessage().getMessageId());
        connectionInfoService.refresh(chatId);
        connectionInfoService.setState(chatId, BotState.LOGIN_EMAIL);
        publisher.publish(update, EventSource.INTERNAL);
    }

    @Override
    public Command command() {
        return Command.REFRESH;
    }
}
