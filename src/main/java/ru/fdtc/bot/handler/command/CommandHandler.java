package ru.fdtc.bot.handler.command;

import org.telegram.telegrambots.meta.api.objects.Update;

public interface CommandHandler {

    void handle(Update update);

    Command command();
}
