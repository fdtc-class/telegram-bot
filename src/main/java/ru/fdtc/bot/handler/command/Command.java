package ru.fdtc.bot.handler.command;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum Command {

    START("/start", "Начать работу (если вы уже авторизованы, возвращает на главную страницу"),
    REFRESH("/refresh", "Очистить кеш пользователя");

    private final String name;
    private final String description;

    public static Command of(String value) {
        return Arrays.stream(values())
                .filter(command -> command.name.equals(value))
                .findFirst()
                .orElse(null);
    }
}
