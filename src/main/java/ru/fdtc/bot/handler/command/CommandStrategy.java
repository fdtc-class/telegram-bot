package ru.fdtc.bot.handler.command;

import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
public class CommandStrategy {

    private final Map<Command, CommandHandler> handlerMap;

    public Optional<CommandHandler> getHandler(Command command) {
        return Optional.ofNullable(handlerMap.get(command));
    }
}
