package ru.fdtc.bot.handler.update.teacher.clazz.check;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.data.entity.HungTaskFormCache;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class CheckTaskHandler extends DefaultUpdateHandler {

    private final TaskInfoService taskInfoService;

    public CheckTaskHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                            UserConnectionInfoService connectionInfoService, TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var file = taskInfoService.getHangedTaskFile(chatId);
        var sendFile = new SendDocument();
        sendFile.setChatId(chatId);
        sendFile.setDocument(file);
        try {
            rememberMessage(getSender().execute(sendFile));
        } catch (TelegramApiException e) {
            log.error("Unable to send file {}", file, e);
        }

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.ESTIMATE),
                Button.of(PreparedButton.DECLINE)
        );

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Оцените или верните выполненное задание");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        var hangedTaskForm = new HungTaskFormCache();
        hangedTaskForm.setTaskPerformanceId(connectionInfo.getOpenHangedTaskId());
        clearMessage(update);
        switch (button.prepared()) {
            case ESTIMATE -> {
                hangedTaskForm.setApprove(true);
                connectionInfo.setState(BotState.ENTER_POINTS);
            }
            case DECLINE -> {
                hangedTaskForm.setApprove(false);
                connectionInfo.setState(BotState.ENTER_COMMENT_TO_TASK);
            }
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CHECK_TASK handler", callbackData, chatId);
        }
        connectionInfo.setHungTaskFormCache(hangedTaskForm);
        getUserConnectionInfoService().save(connectionInfo);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CHECK_TASK;
    }
}
