package ru.fdtc.bot.handler.update.teacher.task.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.time.LocalDate;

@Slf4j
@Component
public class CreateTaskExpiredHandler extends DefaultUpdateHandler {

    public CreateTaskExpiredHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                   UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        sendMessage.setText("Введите дату истечения срока выполнения (например: 2023-06-10):");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        clearMessage(update);

        var chatId = UpdateUtil.getChatId(update);

        LocalDate expiredAt;
        try {
            expiredAt = LocalDate.parse(update.getMessage().getText());
        } catch (Exception e) {
            var sendMessage = new SendMessage();
            sendMessage.setText("Дата введена неправильно, попытайтесь ещё");
            sendMessage.setChatId(chatId);
            rememberMessage(getSender().execute(sendMessage));
            return;
        }

        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        var taskForm = connectionInfo.getTaskForm();
        taskForm.setExpiredAt(expiredAt);
        connectionInfo.setState(BotState.CREATE_TASK_APPROVE);
        getUserConnectionInfoService().save(connectionInfo);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CREATE_TASK_EXPIRED;
    }
}
