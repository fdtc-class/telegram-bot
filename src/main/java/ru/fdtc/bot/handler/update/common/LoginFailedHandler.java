package ru.fdtc.bot.handler.update.common;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Component
public class LoginFailedHandler extends DefaultUpdateHandler {

    public LoginFailedHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                              UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        sendMessage.setText("Не получилось войти, попробуйте ещё раз");
        sendMessage.setChatId(chatId);
        rememberMessage(getSender().execute(sendMessage));

        getUserConnectionInfoService().setState(chatId, BotState.LOGIN_EMAIL);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public void doHandleAfter(Update update) {
        // Does nothing
    }

    @Override
    public BotState botState() {
        return BotState.LOGIN_FAILED;
    }
}
