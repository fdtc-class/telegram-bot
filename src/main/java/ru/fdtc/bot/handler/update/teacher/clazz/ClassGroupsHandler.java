package ru.fdtc.bot.handler.update.teacher.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.rest.dto.response.GetClassMembersRs;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.stream.Collectors;

@Slf4j
@Component
public class ClassGroupsHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public ClassGroupsHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                   UserConnectionInfoService connectionInfoService,
                                   ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var students = classInfoService.findClassPerformance(chatId);
        var groups = students.stream()
                .collect(Collectors.groupingBy(GetClassMembersRs::getGroupName)).keySet();

        var text = new StringBuilder();
        if(groups.isEmpty()) {
            text.append("В данном классе ещё не обучается ни одна группа");
        } else {
            text.append("В данном классе обучаются студенты групп(ы):\n");
            groups.forEach(group -> text.append(group).append("\n"));
        }

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.ADD_GROUP),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(text.toString());
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);

        switch (button.prepared()) {
            case ADD_GROUP -> getUserConnectionInfoService().setState(chatId, BotState.ADD_GROUP_TO_CLASS);
            case TURN_BACK_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CLASS_GROUP handler", data, chatId);
        }
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CLASS_GROUPS;
    }
}
