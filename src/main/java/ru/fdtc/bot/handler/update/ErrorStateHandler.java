package ru.fdtc.bot.handler.update;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Component
public class ErrorStateHandler extends DefaultUpdateHandler {

    public ErrorStateHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                             UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        sendMessage.setText("Произошла неизвестная ошибка. Необходимо перезайти. \nВведите любое сообщение:");
        sendMessage.setChatId(chatId);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        connectionInfo.setState(BotState.LOGIN_EMAIL);
        getUserConnectionInfoService().save(connectionInfo);
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.ERROR;
    }
}
