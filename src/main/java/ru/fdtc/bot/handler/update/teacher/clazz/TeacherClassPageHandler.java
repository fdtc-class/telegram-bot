package ru.fdtc.bot.handler.update.teacher.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TeacherClassPageHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public TeacherClassPageHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                   UserConnectionInfoService connectionInfoService,
                                   ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.CLASS_INFO),
                Button.of(PreparedButton.TASKS),
                Button.of(PreparedButton.MATERIALS),
                Button.of(PreparedButton.CHECK_TASKS),
                Button.of(PreparedButton.STUDENTS_PERFORMANCE),
                Button.of(PreparedButton.GROUPS),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );

        var classInfo = classInfoService.getCurrent(chatId);
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(classInfo.getSubjectName() + " " + classInfo.getClassName());
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        switch (button.prepared()) {
            case CLASS_INFO -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_INFO);
            case TASKS -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_TASKS);
            case MATERIALS -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_MATERIALS);
            case CHECK_TASKS -> getUserConnectionInfoService().setState(chatId, BotState.CHECK_TASKS);
            case STUDENTS_PERFORMANCE -> getUserConnectionInfoService().setState(chatId, BotState.CLASS_PERFORMANCE);
            case GROUPS -> getUserConnectionInfoService().setState(chatId, BotState.CLASS_GROUPS);
            case TURN_BACK_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_MAIN_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in TEACHER_CLASS_PAGE handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_CLASS_PAGE;
    }
}
