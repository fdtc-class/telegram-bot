package ru.fdtc.bot.handler.update.student.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class StudentClassPageHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public StudentClassPageHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                   UserConnectionInfoService connectionInfoService,
                                   ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.CLASS_INFO),
                Button.of(PreparedButton.TASKS),
                Button.of(PreparedButton.MATERIALS),
                Button.of(PreparedButton.CLASS_STATUS),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );

        var classInfo = classInfoService.getCurrent(chatId);
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(classInfo.getSubjectName() + " " + classInfo.getClassName());
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        switch (button.prepared()) {
            case CLASS_INFO -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASS_INFO);
            case TASKS -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_TASKS);
            case MATERIALS -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASS_MATERIALS);
            case CLASS_STATUS -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASS_STATUS);
            case TURN_BACK_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_MAIN_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in STUDENT_CLASS_PAGE handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.STUDENT_CLASS_PAGE;
    }
}
