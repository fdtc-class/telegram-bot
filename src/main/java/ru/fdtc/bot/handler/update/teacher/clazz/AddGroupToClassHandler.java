package ru.fdtc.bot.handler.update.teacher.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.constant.CallbackConstants;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class AddGroupToClassHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public AddGroupToClassHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                        UserConnectionInfoService connectionInfoService,
                                        ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var groups = classInfoService.getGroups(chatId);

        var buttons = groups.stream()
                .map(group -> Button.of(group.getName(), CallbackConstants.GROUP_ID, group.getId()))
                .toList();

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(buttons.toArray(new Button[]{}));

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Все группы:");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);

        getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
        classInfoService.addGroupToClass(chatId, button.payload().get(CallbackConstants.GROUP_ID));
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.ADD_GROUP_TO_CLASS;
    }
}
