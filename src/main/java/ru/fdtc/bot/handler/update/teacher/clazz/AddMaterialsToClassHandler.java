package ru.fdtc.bot.handler.update.teacher.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.io.InputStream;

@Slf4j
@Component
public class AddMaterialsToClassHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public AddMaterialsToClassHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                      UserConnectionInfoService connectionInfoService,
                                      ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        sendMessage.setText("Прикрепите файл с материалом: ");
        sendMessage.setChatId(chatId);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var document = update.getMessage().getDocument();
        var stream = download(document);
        classInfoService.addClassMaterial(chatId, stream, document.getFileName(), document.getMimeType());

        getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.ADD_MATERIALS_TO_CLASS;
    }

    private InputStream download(Document document) throws TelegramApiException {
        var getFileRequest = new GetFile(document.getFileId());
        var downloadRequest = getSender().execute(getFileRequest);

        return getSender().downloadFileAsStream(downloadRequest);
    }
}
