package ru.fdtc.bot.handler.update.admin.teacher.add;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TeacherCreationSuccessHandler extends DefaultUpdateHandler {

    private static final String TEACHER_CREATED_TEXT = """
            Пользователь был успешно создан!
            Данные для входа:
            Почта: %s
            Пароль: %s
            """;

    public TeacherCreationSuccessHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                    UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var user = getUserConnectionInfoService().findById(chatId);
        var teacherForm = user.getTeacherForm();

        var sendMessage = new SendMessage();

        sendMessage.setText(TEACHER_CREATED_TEXT.formatted(teacherForm.getUsername(), teacherForm.getPassword()));
        sendMessage.setChatId(chatId);

        var keyboardMarkup = ButtonUtil.prepareRaw(Button.of(PreparedButton.TURN_BACK_BUTTON));
        sendMessage.setReplyMarkup(keyboardMarkup);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);
        var user = getUserConnectionInfoService().findById(chatId);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            user.setState(BotState.ADMIN_MAIN_PAGE);
            user.setTeacherForm(null);
        } else {
            log.warn("Unable to identify callback data {}, " +
                    "from chat {} in TEACHER_CREATION_SUCCESS handler", data, chatId);
        }


        getUserConnectionInfoService().save(user);
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_CREATION_SUCCESS;
    }
}
