package ru.fdtc.bot.handler.update;

import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.exception.FdtcBotException;

public interface UpdateHandler {

    default void handle(Update update, EventSource eventSource) {
        try {
            switch (eventSource) {
                case INTERNAL -> handleBefore(update);
                case EXTERNAL -> handleAfter(update);
            }
        } catch (Exception e) {
            throw new FdtcBotException(e.getMessage());
        }
    }

    void handleBefore(Update update) throws Exception;
    default void handleAfter(Update update) throws Exception {
        doHandleAfter(update);
    }

    void doHandleBefore(Update update) throws Exception;

    void doHandleAfter(Update update) throws Exception;

    BotState botState();

    void clearMessage(Update update) throws TelegramApiException;

    void rememberMessage(Message message);
}
