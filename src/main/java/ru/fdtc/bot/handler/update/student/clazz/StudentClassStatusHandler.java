package ru.fdtc.bot.handler.update.student.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class StudentClassStatusHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;
    private static final String CLASS_STATUS_TEXT = """
            Вы набрали %s балл(ов) из %s возможных
            """;

    public StudentClassStatusHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                     UserConnectionInfoService connectionInfoService,
                                     ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var classInfo = classInfoService.getCurrent(chatId);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareRaw(Button.of(PreparedButton.TURN_BACK_BUTTON));
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(CLASS_STATUS_TEXT.formatted(classInfo.getCurrentPoints(), classInfo.getRequiredPoints()));
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = update.getCallbackQuery().getData();
        var button = Button.of(callbackData);
        clearMessage(update);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASS_PAGE);
        } else {
            log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CLASS_INFO handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.STUDENT_CLASS_STATUS;
    }
}
