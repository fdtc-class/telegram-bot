package ru.fdtc.bot.handler.update.teacher.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class ClassPerformanceHandler extends DefaultUpdateHandler {

    private static final String PERFORMANCE_TEXT = """
            %s %s - %s баллов из %s
            %s
            """;

    private final ClassInfoService classInfoService;

    public ClassPerformanceHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                 UserConnectionInfoService connectionInfoService,
                                 ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var students = classInfoService.findClassPerformance(chatId);
        var clazz = classInfoService.getCurrent(chatId);

        students.forEach(student -> {
            var message = SendMessage.builder()
                    .text(PERFORMANCE_TEXT.formatted(student.getLastName(), student.getFirstName(),
                            student.getCurrentPoints(), clazz.getRequiredPoints(), student.getGroupName()))
                    .chatId(chatId)
                    .build();

            try {
                rememberMessage(getSender().execute(message));
            } catch (TelegramApiException e) {
                log.error("Unable to send performance message", e);
            }
        });

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(Button.of(PreparedButton.TURN_BACK_BUTTON));
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Успеваемость класса");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
        } else {
            log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CLASS_PERFORMANCE handler", data, chatId);
        }
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CLASS_PERFORMANCE;
    }
}
