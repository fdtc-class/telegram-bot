package ru.fdtc.bot.handler.update.teacher.task.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class CreateTaskApproveHandler extends DefaultUpdateHandler {

    private static final String TASK_APPROVE_TEXT = """
            Проверьте корректность введённых данных:
            Номер: %s
            Название: %s
            Описание: %s
            Макс. баллов: %s
            Дата закрытия: %s
            """;

    private final TaskInfoService taskInfoService;

    public CreateTaskApproveHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                    UserConnectionInfoService connectionInfoService,
                                    TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var user = getUserConnectionInfoService().findById(chatId);
        var taskForm = user.getTaskForm();

        var sendMessage = new SendMessage();
        sendMessage.setText(TASK_APPROVE_TEXT.formatted(
                taskForm.getNumber(),
                taskForm.getName(),
                taskForm.getDescription(),
                taskForm.getMaximumPoints(),
                taskForm.getExpiredAt()
        ));
        sendMessage.setChatId(chatId);

        var keyboardMarkup = ButtonUtil.prepareRaw(
                Button.of(PreparedButton.APPROVE_FORM),
                Button.of(PreparedButton.REFILL_FORM)
        );
        sendMessage.setReplyMarkup(keyboardMarkup);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);

        switch (button.prepared()) {
            case APPROVE_FORM -> {
                taskInfoService.create(chatId);
                getUserConnectionInfoService().setState(chatId, BotState.TASK_CREATED);
            }
            case REFILL_FORM -> {
                getUserConnectionInfoService().setState(chatId, BotState.CREATE_TASK_NUMBER);
                getUserConnectionInfoService().clearForms(chatId);
            }
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CREATE_TASK_APPROVE handler", data, chatId);
        }
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CREATE_TASK_APPROVE;
    }
}
