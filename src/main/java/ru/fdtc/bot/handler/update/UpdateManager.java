package ru.fdtc.bot.handler.update;

import io.vavr.control.Try;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.data.entity.UserConnectionInfo;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
@RequiredArgsConstructor
public class UpdateManager {

    private final HandlerStrategy strategy;
    private final UserConnectionInfoService userConnectionInfoService;
    private final UpdateEventPublisher publisher;

    @Transactional
    public void manage(Update update, EventSource eventSource) {
        var chatId = UpdateUtil.getChatId(update);
        var connectionInfo = userConnectionInfoService.findById(chatId);
        var handler = strategy.getHandler(connectionInfo.getState());

        if(handler.isPresent()) {
            Try.run(() -> handler.get().handle(update, eventSource))
                    .onFailure(t -> {
                        log.error("Unable to handle {}. Reason: {}", handler.get().botState(), t.getMessage(), t);
                        handleError(connectionInfo, update);
                    });

        } else {
            log.warn("No handler found for such state: {}", connectionInfo.getState());
            handleError(connectionInfo, update);
        }
    }

    private void handleError(UserConnectionInfo connectionInfo, Update update) {
        connectionInfo.setState(BotState.ERROR);
        connectionInfo.clearCachedInfo();
        userConnectionInfoService.save(connectionInfo);
        publisher.publish(update, EventSource.INTERNAL);
    }
}
