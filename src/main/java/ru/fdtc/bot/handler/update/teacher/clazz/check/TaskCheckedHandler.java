package ru.fdtc.bot.handler.update.teacher.clazz.check;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TaskCheckedHandler extends DefaultUpdateHandler {

    public TaskCheckedHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.TURN_BACK_TO_CLASS_PERFORMANCE),
                Button.of(PreparedButton.TURN_BACK_TO_CLASS)
        );

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Работа проверена");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        connectionInfo.setHungTaskFormCache(null);
        getUserConnectionInfoService().save(connectionInfo);

        var button = Button.of(data);
        switch (button.prepared()) {
            case TURN_BACK_TO_CLASS_PERFORMANCE -> getUserConnectionInfoService().setState(chatId, BotState.CHECK_TASKS);
            case TURN_BACK_TO_CLASS -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CHECK_TASK handler", data, chatId);
        }

        clearMessage(update);
        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TASK_CHECKED;
    }
}
