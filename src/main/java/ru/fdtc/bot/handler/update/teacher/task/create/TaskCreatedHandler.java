package ru.fdtc.bot.handler.update.teacher.task.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TaskCreatedHandler extends DefaultUpdateHandler {

    private static final String TASK_CREATED_TEXT = "Задание №%s \"%s\" успешно создано!\n";

    public TaskCreatedHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                              UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var user = getUserConnectionInfoService().findById(chatId);
        var taskForm = user.getTaskForm();

        var sendMessage = new SendMessage();

        sendMessage.setText(TASK_CREATED_TEXT.formatted(taskForm.getNumber(), taskForm.getName()));
        sendMessage.setChatId(chatId);

        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.GET_TO_TASK),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );
        sendMessage.setReplyMarkup(keyboardMarkup);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);
        var user = getUserConnectionInfoService().findById(chatId);


        switch (button.prepared()) {
            case GET_TO_TASK -> user.setState(BotState.TEACHER_TASK_PAGE);
            case TURN_BACK_BUTTON -> user.setState(BotState.TEACHER_CLASS_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                                "from chat {} in TASK_CREATED handler", data, chatId);
        }
        user.setTaskForm(null);
        getUserConnectionInfoService().save(user);
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TASK_CREATED;
    }
}
