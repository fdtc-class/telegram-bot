package ru.fdtc.bot.handler.update.teacher.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.constant.CallbackConstants;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
public class TeacherTasksHandler extends DefaultUpdateHandler {

    private static final String BUTTON_NAME = "#%s %s (%s)";

    private final TaskInfoService taskInfoService;

    public TeacherTasksHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                               UserConnectionInfoService connectionInfoService,
                               TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var tasks = taskInfoService.findTasks(chatId);

        var buttons = tasks.stream()
                .map(task -> Button.of(BUTTON_NAME.formatted(task.getNumber(), task.getName(), task.getStatus()),
                        CallbackConstants.TASK_ID, task.getId()))
                .collect(Collectors.toList());

        var addTaskButton = Button.of(PreparedButton.ADD_TASK);
        var turnBackButton = Button.of(PreparedButton.TURN_BACK_BUTTON);
        buttons.add(addTaskButton);
        buttons.add(turnBackButton);

        var sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);

        if(tasks.isEmpty()) {
            sendMessage.setText("Вы ещё не добавили ни одного задания");
        } else {
            sendMessage.setText("Задания");
        }

        var keyboardMarkup = ButtonUtil.prepareColumn(buttons.toArray(new Button[]{}));
        sendMessage.setReplyMarkup(keyboardMarkup);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        if(Objects.nonNull(button.prepared())) {
            switch (button.prepared()) {
                case ADD_TASK -> getUserConnectionInfoService().setState(chatId, BotState.CREATE_TASK_NUMBER);
                case TURN_BACK_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
                default -> log.warn("Unable to identify callback data {}, " +
                        "from chat {} in TEACHER_TASKS handler", callbackData, chatId);
            }
        } else {
            var user = getUserConnectionInfoService().findById(chatId);
            user.setOpenTaskId(button.payload().get(CallbackConstants.TASK_ID));
            user.setState(BotState.TEACHER_TASK_PAGE);
            getUserConnectionInfoService().save(user);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_TASKS;
    }
}
