package ru.fdtc.bot.handler.update.admin.teacher.add;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.enumeration.UserRole;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.AdminService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TeacherSubmissionHandler extends DefaultUpdateHandler {

    private static final String TEACHER_SUB_TEXT = """
            Проверьте корректность введённых данных:
            Имя: %s
            Фамилия: %s
            Почта: %s
            Пароль: %s
            Администратор: %s
            """;

    private final AdminService adminService;

    public TeacherSubmissionHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                    UserConnectionInfoService connectionInfoService, AdminService adminService) {
        super(sender, publisher, connectionInfoService);
        this.adminService = adminService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var user = getUserConnectionInfoService().findById(chatId);
        var teacherForm = user.getTeacherForm();

        var sendMessage = new SendMessage();

        sendMessage.setText(TEACHER_SUB_TEXT.formatted(
                teacherForm.getFirstName(),
                teacherForm.getLastName(),
                teacherForm.getUsername(),
                teacherForm.getPassword(),
                isAdmin(teacherForm.getRole())
        ));
        sendMessage.setChatId(chatId);

        var keyboardMarkup = ButtonUtil.prepareRaw(
                Button.of(PreparedButton.APPROVE_FORM),
                Button.of(PreparedButton.REFILL_FORM)
        );
        sendMessage.setReplyMarkup(keyboardMarkup);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);
        var user = getUserConnectionInfoService().findById(chatId);

        switch (button.prepared()) {
            case APPROVE_FORM -> {
                var password = adminService.addTeacher(chatId);
                user.getTeacherForm().setPassword(password);
                user.setState(BotState.TEACHER_CREATION_SUCCESS);
            }
            case REFILL_FORM -> {
                user.setState(BotState.TEACHER_NAME);
                user.setTeacherForm(null);
            }
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in TEACHER_ROLE handler", data, chatId);
        }


        getUserConnectionInfoService().save(user);
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_SUBMISSION;
    }

    private String isAdmin(UserRole role) {
        return UserRole.ADMIN.equals(role) ? "Да" : "Нет";
    }
}
