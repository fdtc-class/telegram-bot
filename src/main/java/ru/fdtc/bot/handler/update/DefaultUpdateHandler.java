package ru.fdtc.bot.handler.update;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.retry.annotation.Retryable;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.ActionType;
import org.telegram.telegrambots.meta.api.methods.send.SendChatAction;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.List;

@Slf4j
@Getter
@RequiredArgsConstructor
public abstract class DefaultUpdateHandler implements UpdateHandler {

    private final DefaultAbsSender sender;
    private final UpdateEventPublisher publisher;
    private final UserConnectionInfoService userConnectionInfoService;

    @Override
    public void handleBefore(Update update) throws Exception {
        long chatId = UpdateUtil.getChatId(update);
        var action = SendChatAction.builder()
                .chatId(chatId)
                .action(ActionType.TYPING.toString())
                .build();
        sender.execute(action);
        doHandleBefore(update);
    }

    @Override
    public void clearMessage(Update update) throws TelegramApiException {
        long chatId = UpdateUtil.getChatId(update);
        int messageId = UpdateUtil.getMessageId(update);
        var connectionInfo = userConnectionInfoService.findById(chatId);
        var sentMessages = connectionInfo.getSentMessages();

        if (!sentMessages.contains(messageId)) {
            deleteReceivedMessage(chatId, messageId);
        }
        connectionInfo.setSentMessages(deleteSentMessage(sentMessages,chatId));
        userConnectionInfoService.save(connectionInfo);
    }

    @Override
    public void rememberMessage(Message message) {
        long chatId = message.getChatId();
        int messageId = message.getMessageId();
        var connectionInfo = userConnectionInfoService.findById(chatId);
        connectionInfo.getSentMessages().add(messageId);
        userConnectionInfoService.save(connectionInfo);
        log.debug("Message '{}' is saved", messageId);
    }

    private void deleteReceivedMessage(long chatId, int messageId) throws TelegramApiException {
        deleteMessage(chatId, messageId);
        log.debug("Received message '{}' is removed", messageId);
    }

    private List<Integer> deleteSentMessage(List<Integer> sentMessages, long chatId) {
        return sentMessages.stream()
                .filter(messageId -> {
                    try {
                        deleteMessage(chatId, messageId);
                        log.debug("Sent message '{}' is removed", sentMessages);
                        return false;
                    } catch (TelegramApiException e) {
                        return true;
                    }
                }).toList();
    }

    @Retryable
    public void deleteMessage(long chatId, int messageId) throws TelegramApiException {
        var deleteMessage = DeleteMessage.builder()
                .chatId(chatId)
                .messageId(messageId)
                .build();
        sender.execute(deleteMessage);
    }
}
