package ru.fdtc.bot.handler.update.common;

import io.vavr.control.Try;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendSticker;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.AuthorizationService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Component
public class LoginPasswordHandler extends DefaultUpdateHandler {

    private final AuthorizationService authorizationService;

    public LoginPasswordHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                UserConnectionInfoService connectionInfoService,
                                AuthorizationService authorizationService) {
        super(sender, publisher, connectionInfoService);
        this.authorizationService = authorizationService;
    }

    @Override
    public void doHandleBefore(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        sendMessage.setText("Введите пароль: ");
        sendMessage.setChatId(chatId);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var password = update.getMessage().getText();
        var email = getUserConnectionInfoService().findById(chatId).getEmail();
        sendWaitingMessage(chatId);

        Try.run(() -> {
            var loginResponse = authorizationService.login(email, password);
            getUserConnectionInfoService().setToken(chatId, loginResponse.getToken());
            getUserConnectionInfoService().setRole(chatId, loginResponse.getRole());

            switch (loginResponse.getRole()) {
                case STUDENT -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_MAIN_PAGE);
                case TEACHER -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_MAIN_PAGE);
                case ADMIN -> getUserConnectionInfoService().setState(chatId, BotState.ADMIN_CHOOSE_ROLE);
            }
        }).onFailure(ex -> getUserConnectionInfoService().setState(chatId, BotState.LOGIN_FAILED));
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.LOGIN_PASSWORD;
    }

    private void sendWaitingMessage(Long chatId) throws TelegramApiException {
        var message = new SendMessage();
        message.setChatId(chatId);
        message.setText("Ждём ответа от Eservice...");
        rememberMessage(getSender().execute(message));

        var sticker = new SendSticker();
        sticker.setChatId(chatId);
        sticker.setSticker(new InputFile(getClass().getClassLoader().getResourceAsStream("sticker/eservice_waiting.webp"), "eservice_waiting.webp"));
        rememberMessage(getSender().execute(sticker));
    }
}
