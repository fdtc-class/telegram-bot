package ru.fdtc.bot.handler.update.teacher.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.io.InputStream;

@Slf4j
@Component
public class AddFileToTaskHandler extends DefaultUpdateHandler {

    private final TaskInfoService taskInfoService;

    public AddFileToTaskHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                UserConnectionInfoService connectionInfoService,
                                TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        sendMessage.setText("Прикрепите файл с заданием: ");
        sendMessage.setChatId(chatId);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        clearMessage(update);
        var chatId = UpdateUtil.getChatId(update);

        if(update.getMessage().hasDocument()) {
            var document = update.getMessage().getDocument();
            var stream = downloadFile(document);
            taskInfoService.addTaskMaterial(chatId, stream, document.getFileName(), document.getMimeType());
            getUserConnectionInfoService().setState(chatId, BotState.TEACHER_TASK_FILE);
        } else {
            rememberMessage(getSender().execute(SendMessage.builder()
                            .chatId(chatId)
                            .text("Отправлять можно только документы!")
                    .build()));
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.ADD_FILE_TO_TASK;
    }

    private InputStream downloadFile(Document document) throws TelegramApiException {
        var getFileRequest = new GetFile(document.getFileId());
        var downloadRequest = getSender().execute(getFileRequest);

        return getSender().downloadFileAsStream(downloadRequest);
    }
}
