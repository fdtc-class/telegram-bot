package ru.fdtc.bot.handler.update.admin;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class AdminMainPageHandler extends DefaultUpdateHandler {

    public AdminMainPageHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareRaw(
                Button.of(PreparedButton.ADD_TEACHER),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Главное меню");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        switch (button.prepared()) {
            case ADD_TEACHER -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_NAME);
            case TURN_BACK_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.ADMIN_CHOOSE_ROLE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in STUDENT_MAIN_PAGE handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.ADMIN_MAIN_PAGE;
    }
}
