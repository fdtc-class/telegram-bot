package ru.fdtc.bot.handler.update.teacher.clazz.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class SubjectDescriptionHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public SubjectDescriptionHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                     UserConnectionInfoService connectionInfoService,
                                     ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        sendMessage.setText("Введите описание предмета:");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        clearMessage(update);
        var chatId = UpdateUtil.getChatId(update);
        var subjectDescription = update.getMessage().getText();

        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        var subject = connectionInfo.getSubjectForm();
        subject.setDescription(subjectDescription);
        classInfoService.addSubject(chatId);
        connectionInfo.setState(BotState.SUBJECT_CREATED);
        getUserConnectionInfoService().save(connectionInfo);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.SUBJECT_DESCRIPTION;
    }
}
