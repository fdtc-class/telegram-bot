package ru.fdtc.bot.handler.update.student;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class StudentMainPageHandler extends DefaultUpdateHandler {

    public StudentMainPageHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareRaw(
                Button.of(PreparedButton.APP_INFO_BUTTON),
                Button.of(PreparedButton.ACTUAL_CLASSES_BUTTON)
        );
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Главное меню");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        switch (button.prepared()) {
            case APP_INFO_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.APP_INFO);
            case ACTUAL_CLASSES_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASSES);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in STUDENT_MAIN_PAGE handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.STUDENT_MAIN_PAGE;
    }
}
