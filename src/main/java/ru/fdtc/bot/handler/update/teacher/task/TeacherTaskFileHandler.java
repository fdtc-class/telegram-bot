package ru.fdtc.bot.handler.update.teacher.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.Objects;

@Slf4j
@Component
public class TeacherTaskFileHandler extends DefaultUpdateHandler {

    private final TaskInfoService taskInfoService;

    public TeacherTaskFileHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService,
                                  TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.ADD_TASK_FILE),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );
        sendMessage.setChatId(chatId);
        sendMessage.setReplyMarkup(keyboardMarkup);

        var file = taskInfoService.getMaterial(chatId);
        if(Objects.nonNull(file)) {
            var sendFile = new SendDocument();
            sendFile.setChatId(chatId);
            sendFile.setDocument(file);
            sendMessage.setText("Файл с заданием:");
            try {
                rememberMessage(getSender().execute(sendFile));
            } catch (TelegramApiException e) {
                log.error("Unable to send file {}", file, e);
            }
        } else {
            sendMessage.setText("Нет прикреплённых файлов");
        }

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        switch (button.prepared()) {
            case ADD_TASK_FILE -> getUserConnectionInfoService().setState(chatId, BotState.ADD_FILE_TO_TASK);
            case TURN_BACK_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_TASK_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in TEACHER_TASK_FILE handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_TASK_FILE;
    }
}
