package ru.fdtc.bot.handler.update.teacher.clazz.check;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class EnterPointsHandler extends DefaultUpdateHandler {

    public EnterPointsHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                            UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        sendMessage.setText("Введите количество баллов:");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        clearMessage(update);
        var chatId = UpdateUtil.getChatId(update);
        var points = Integer.parseInt(update.getMessage().getText());
        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        var hangedTaskForm = connectionInfo.getHungTaskFormCache();
        hangedTaskForm.setPoints(points);
        connectionInfo.setState(BotState.ENTER_COMMENT_TO_TASK);
        getUserConnectionInfoService().save(connectionInfo);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.ENTER_POINTS;
    }
}