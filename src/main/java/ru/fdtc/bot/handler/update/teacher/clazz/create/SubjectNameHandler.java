package ru.fdtc.bot.handler.update.teacher.clazz.create;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.data.entity.SubjectFormCache;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Component
public class SubjectNameHandler extends DefaultUpdateHandler {

    public SubjectNameHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                     UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        sendMessage.setText("Введите название предмета:");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        clearMessage(update);
        var chatId = UpdateUtil.getChatId(update);
        var subjectName = update.getMessage().getText();

        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        var subject = SubjectFormCache.builder().name(subjectName).build();
        connectionInfo.setSubjectForm(subject);

        connectionInfo.setState(BotState.SUBJECT_DESCRIPTION);
        getUserConnectionInfoService().save(connectionInfo);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.SUBJECT_NAME;
    }
}
