package ru.fdtc.bot.handler.update.teacher.task.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.data.entity.TaskFormCache;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class CreateTaskNumberHandler extends DefaultUpdateHandler {

    private final TaskInfoService taskInfoService;

    public CreateTaskNumberHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                   UserConnectionInfoService connectionInfoService,
                                   TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var sendMessage = new SendMessage();
        sendMessage.setText("Введите номер задания:");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        clearMessage(update);

        var chatId = UpdateUtil.getChatId(update);
        var taskNumber = Integer.parseInt(update.getMessage().getText());

        if(taskInfoService.isNumberExist(chatId, taskNumber)) {
            var sendMessage = new SendMessage();
            sendMessage.setText("Задание с таким номером уже существует");
            sendMessage.setChatId(chatId);
            rememberMessage(getSender().execute(sendMessage));
        } else {
            var connectionInfo = getUserConnectionInfoService().findById(chatId);
            var taskForm = TaskFormCache.builder()
                    .number(taskNumber)
                    .classId(connectionInfo.getOpenClassId())
                    .build();
            connectionInfo.setTaskForm(taskForm);
            connectionInfo.setState(BotState.CREATE_TASK_NAME);
            getUserConnectionInfoService().save(connectionInfo);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CREATE_TASK_NUMBER;
    }
}
