package ru.fdtc.bot.handler.update.admin.teacher.add;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TeacherPasswordHandler extends DefaultUpdateHandler {

    public TeacherPasswordHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                               UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        sendMessage.setText("Введите пароль для преподавателя (или просто нажмите “Сгенерировать автоматически”):");
        sendMessage.setChatId(chatId);

        var keyboardMarkup = ButtonUtil.prepareRaw(Button.of(PreparedButton.GENERATE_PASSWORD_AUTO));
        sendMessage.setReplyMarkup(keyboardMarkup);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var user = getUserConnectionInfoService().findById(chatId);
        var teacherForm = user.getTeacherForm();

        if(update.hasMessage()) {
            var password = update.getMessage().getText();
            teacherForm.setPassword(password);
        } else if (update.hasCallbackQuery()) {
            var data = update.getCallbackQuery().getData();
            var button = Button.of(data);
            if(PreparedButton.GENERATE_PASSWORD_AUTO.equals(button.prepared())) {
                teacherForm.setPassword(null);
            } else {
                log.warn("Unable to identify callback data {}, " +
                        "from chat {} in TEACHER_PASSWORD handler", data, chatId);
                return;
            }
        } else {
            log.warn("Unable to identify update data in TEACHER_PASSWORD handler, update {}", update);
            return;
        }

        user.setState(BotState.TEACHER_ROLE);
        getUserConnectionInfoService().save(user);
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_PASSWORD;
    }
}
