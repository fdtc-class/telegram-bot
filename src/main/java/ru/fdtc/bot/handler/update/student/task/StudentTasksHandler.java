package ru.fdtc.bot.handler.update.student.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.constant.CallbackConstants;
import ru.fdtc.bot.common.constant.Emoji;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.rest.dto.enumeration.TaskPerformanceStatus;
import ru.fdtc.bot.rest.dto.enumeration.TaskStatus;
import ru.fdtc.bot.rest.dto.response.TaskShortInfo;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class StudentTasksHandler extends DefaultUpdateHandler {

    private static final String TASK_TEXT = "#%s %s %s";

    private final TaskInfoService taskInfoService;

    public StudentTasksHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                               UserConnectionInfoService connectionInfoService,
                               TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var tasks = taskInfoService.findTasks(chatId);

        var buttons = tasks.stream()
                .map(task -> {
                    var text = TASK_TEXT.formatted(task.getNumber(), task.getName(), getEmoji(task));
                    return Button.of(text, CallbackConstants.TASK_ID, task.getId());
                })
                .toList();

        var sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);

        if(buttons.isEmpty()) {
            sendMessage.setText("Преподаватель ещё не успел добавить задания, загляните в этот раздел чуть позже)");
            var keyboardMarkup = ButtonUtil.prepareRaw(Button.of(PreparedButton.TURN_BACK_BUTTON));
            sendMessage.setReplyMarkup(keyboardMarkup);
        } else {
            sendMessage.setText("Задания");
            var keyboardMarkup = ButtonUtil.prepareColumn(buttons.toArray(new Button[]{}));
            sendMessage.setReplyMarkup(keyboardMarkup);
        }

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var role = getUserConnectionInfoService().findById(chatId).getRole();
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        if(PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            switch (role) {
                case STUDENT -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASS_PAGE);
                case TEACHER -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
                case ADMIN -> getUserConnectionInfoService().setState(chatId, BotState.ADMIN_CLASS_PAGE);
            }
        } else {
            taskInfoService.save(chatId, button.payload().get(CallbackConstants.TASK_ID));
            switch (role) {
                case STUDENT -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_TASK_PAGE);
                case TEACHER -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_TASK_PAGE);
                case ADMIN -> getUserConnectionInfoService().setState(chatId, BotState.ADMIN_TASK_PAGE);
            }
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.STUDENT_TASKS;
    }

    private String getEmoji(TaskShortInfo task) {
        if(task.getPerformance().getStatus() == TaskPerformanceStatus.CONFIRM) {
            return Emoji.NICE;
        } else {
            if (task.getStatus() == TaskStatus.EXPIRED) {
                return Emoji.RED_CIRCLE;
            } else if(task.getStatus() == TaskStatus.ACTIVE) {
                if(task.getPerformance().getStatus() == TaskPerformanceStatus.ON_CHECK) {
                    return Emoji.CLOCK;
                }

                if(task.getPerformance().getStatus() == TaskPerformanceStatus.DECLINED) {
                    return Emoji.BANNED;
                }
                return Emoji.GREEN_CIRCLE;
            } else {
                return Emoji.WHITE_CIRCLE;
            }
        }
    }
}
