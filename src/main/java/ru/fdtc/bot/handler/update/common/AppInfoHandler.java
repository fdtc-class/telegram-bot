package ru.fdtc.bot.handler.update.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.config.BotConfig;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.List;

@Slf4j
@Component
public class AppInfoHandler extends DefaultUpdateHandler {

    private final BotConfig botConfig;

    public AppInfoHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                          UserConnectionInfoService connectionInfoService,
                          BotConfig botConfig) {
        super(sender, publisher, connectionInfoService);
        this.botConfig = botConfig;
    }

    @Override
    public void doHandleBefore(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepare(List.of(Button.of(PreparedButton.TURN_BACK_BUTTON)));
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(botConfig.getInfo());
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        var connectionInfo = getUserConnectionInfoService().findById(chatId);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            switch (connectionInfo.getRole()) {
                case STUDENT -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_MAIN_PAGE);
                case TEACHER, ADMIN -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_MAIN_PAGE);
            }
        } else {
            log.warn("Unable to identify callback data {}, " +
                    "from chat {} in APP_INFO handler", callbackData, chatId);
        }
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.APP_INFO;
    }
}
