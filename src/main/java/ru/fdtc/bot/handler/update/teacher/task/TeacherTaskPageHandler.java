package ru.fdtc.bot.handler.update.teacher.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.rest.dto.enumeration.TaskStatus;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.ArrayList;

@Slf4j
@Component
public class TeacherTaskPageHandler extends DefaultUpdateHandler {

    private static final String HEADER = "#%s %s (%s)";

    private final TaskInfoService taskInfoService;

    public TeacherTaskPageHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService,
                                  TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        var buttons = new ArrayList<Button>();
        buttons.add(Button.of(PreparedButton.TASK_INFO));
        buttons.add(Button.of(PreparedButton.TASK_FILES));
        buttons.add(Button.of(PreparedButton.TURN_BACK_TO_CLASS));

        var task = taskInfoService.getCurrent(chatId);
        if(!TaskStatus.ACTIVE.equals(task.getStatus())) {
            buttons.add(Button.of(PreparedButton.ACTIVATE_TASK));
        }

        var keyboardMarkup = ButtonUtil.prepareColumn(buttons.toArray(Button[]::new));
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(HEADER.formatted(task.getNumber(), task.getName(), task.getStatus()));
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);
        clearMessage(update);

        switch (button.prepared()) {
            case TASK_INFO -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_TASK_INFO);
            case TASK_FILES -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_TASK_FILE);
            case TURN_BACK_TO_CLASS -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
            case ACTIVATE_TASK -> taskInfoService.activate(chatId);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in STUDENT_TASK_PAGE handler", data, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_TASK_PAGE;
    }
}
