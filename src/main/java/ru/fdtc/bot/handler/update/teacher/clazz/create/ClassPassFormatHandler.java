package ru.fdtc.bot.handler.update.teacher.clazz.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.rest.dto.enumeration.PassFormat;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class ClassPassFormatHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public ClassPassFormatHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService, ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);

        var keyboard = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.EXAM),
                Button.of(PreparedButton.TEST),
                Button.of(PreparedButton.DIFF_TEST)
        );
        var sendMessage = new SendMessage();
        sendMessage.setText("Выберите формат сдачи: ");
        sendMessage.setReplyMarkup(keyboard);
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        clearMessage(update);
        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);
        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        var clazz = connectionInfo.getClassForm();

        switch (button.prepared()) {
            case EXAM -> clazz.setPassFormat(PassFormat.EXAM);
            case TEST -> clazz.setPassFormat(PassFormat.TEST);
            case DIFF_TEST -> clazz.setPassFormat(PassFormat.DIFF_TEST);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CLASS_PASS_FORMAT handler", data, chatId);
        }
        connectionInfo.setState(BotState.CLASS_CREATED);
        getUserConnectionInfoService().save(connectionInfo);
        classInfoService.create(chatId);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CLASS_PASS_FORMAT;
    }
}
