package ru.fdtc.bot.handler.update.teacher.clazz.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.constant.CallbackConstants;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.data.entity.ClassFormCache;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
@Component
public class ChooseSubjectHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public ChooseSubjectHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService,
                                  ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var subjects = classInfoService.getSubjects(chatId);

        var buttons = subjects.stream()
                .map(subject -> Button.of(subject.getName(), CallbackConstants.SUBJECT_ID, subject.getId()))
                .collect(Collectors.toList());

        var addSubjectButton = Button.of(PreparedButton.ADD_SUBJECT);
        buttons.add(addSubjectButton);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(buttons.toArray(new Button[]{}));

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Выберите предмет:");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);

        if(Objects.nonNull(button.prepared()) && PreparedButton.ADD_SUBJECT.equals(button.prepared())) {
            getUserConnectionInfoService().setState(chatId, BotState.SUBJECT_NAME);
        } else {
            var connectionInfo = getUserConnectionInfoService().findById(chatId);
            connectionInfo.setState(BotState.CLASS_NAME);
            connectionInfo.setClassForm(ClassFormCache.builder()
                    .subjectId(button.payload().get(CallbackConstants.SUBJECT_ID))
                    .build());
            getUserConnectionInfoService().save(connectionInfo);
        }
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CHOOSE_SUBJECT;
    }
}
