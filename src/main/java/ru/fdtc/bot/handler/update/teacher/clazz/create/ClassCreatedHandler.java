package ru.fdtc.bot.handler.update.teacher.clazz.create;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class ClassCreatedHandler extends DefaultUpdateHandler {

    public ClassCreatedHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                 UserConnectionInfoService connectionInfoService) {
        super(sender, publisher, connectionInfoService);
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.GET_TO_CLASS),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );
        var clazz = getUserConnectionInfoService().findById(chatId).getClassForm();

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Поздравляем!\nКласс \"%s\" успешно создан!".formatted(clazz.getName()));
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        connectionInfo.setClassForm(null);

        var button = Button.of(data);
        switch (button.prepared()){
            case GET_TO_CLASS -> connectionInfo.setState(BotState.TEACHER_CLASS_PAGE);
            case TURN_BACK_BUTTON -> connectionInfo.setState(BotState.TEACHER_MAIN_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CLASS_CREATED handler", data, chatId);
        }
        getUserConnectionInfoService().save(connectionInfo);

        clearMessage(update);
        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CLASS_CREATED;
    }
}
