package ru.fdtc.bot.handler.update.student.task;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Document;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.io.InputStream;

@Component
public class StudentSubmitTaskHandler extends DefaultUpdateHandler {

    private final TaskInfoService taskInfoService;

    public StudentSubmitTaskHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                    UserConnectionInfoService connectionInfoService, TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var sendMessage = new SendMessage();
        var keyboard = ButtonUtil.prepareColumn(Button.of(PreparedButton.TURN_BACK_BUTTON));
        sendMessage.setReplyMarkup(keyboard);
        sendMessage.setText("Прикрепите файл с выполенным заданием: ");
        sendMessage.setChatId(chatId);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);

        getUserConnectionInfoService().setState(chatId, BotState.STUDENT_TASK_PAGE);
        if(!PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            var document = update.getMessage().getDocument();
            var stream = download(document);
            taskInfoService.submit(chatId, stream, document.getFileName(), document.getMimeType());
        }

        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TASK_SUBMISSION;
    }

    private InputStream download(Document document) throws TelegramApiException {
        var getFileRequest = new GetFile(document.getFileId());
        var downloadRequest = getSender().execute(getFileRequest);

        return getSender().downloadFileAsStream(downloadRequest);
    }
}
