package ru.fdtc.bot.handler.update.teacher.clazz.check;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.constant.CallbackConstants;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.stream.Collectors;

@Slf4j
@Component
public class CheckTasksHandler extends DefaultUpdateHandler {

    private static final String HANGED_TASK_TEXT = """
            Задание #%s - %s %s
            """;
    private final TaskInfoService taskInfoService;

    public CheckTasksHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                             UserConnectionInfoService connectionInfoService,
                             TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var hangTasks = taskInfoService.findHangedTasks(chatId);

        var buttons = hangTasks.stream()
                .map(hangTask -> {
                    var text = HANGED_TASK_TEXT.formatted(hangTask.getTaskNumber(), hangTask.getStudentLastName(), hangTask.getStudentFirstName());
                    return Button.of(text, CallbackConstants.HANGED_TASK_ID, hangTask.getId());
                })
                .collect(Collectors.toList());

        var turnBackButton = Button.of(PreparedButton.TURN_BACK_BUTTON);
        buttons.add(turnBackButton);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareColumn(buttons.toArray(new Button[]{}));

        sendMessage.setReplyMarkup(keyboardMarkup);
        if(hangTasks.isEmpty()) {
            sendMessage.setText("Нет заданий на проверку");
        } else {
            sendMessage.setText("Проверить задания");
        }

        sendMessage.setChatId(chatId);
        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var button = Button.of(data);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
        } else {
            getUserConnectionInfoService().setState(chatId, BotState.CHECK_TASK);
            taskInfoService.saveHanged(chatId, button.payload().get(CallbackConstants.HANGED_TASK_ID));
        }
        clearMessage(update);

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.CHECK_TASKS;
    }

}
