package ru.fdtc.bot.handler.update.teacher.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TeacherClassMaterialsHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public TeacherClassMaterialsHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                        UserConnectionInfoService connectionInfoService,
                                        ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var files = classInfoService.getClassMaterials(chatId);

        files.forEach(file -> {
            var sendFile = new SendDocument();
            sendFile.setChatId(chatId);
            sendFile.setDocument(file);
            try {
                rememberMessage(getSender().execute(sendFile));
            } catch (TelegramApiException e) {
                log.error("Unable to send file {}", file, e);
            }
        });

        var keyboardMarkup = ButtonUtil.prepareColumn(
                Button.of(PreparedButton.ADD_MATERIAL),
                Button.of(PreparedButton.TURN_BACK_BUTTON)
        );

        var sendMessage = SendMessage.builder()
                .chatId(chatId)
                .text("Материалы класса")
                .replyMarkup(keyboardMarkup)
                .build();

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        switch (button.prepared()) {
            case ADD_MATERIAL -> getUserConnectionInfoService().setState(chatId, BotState.ADD_MATERIALS_TO_CLASS);
            case TURN_BACK_BUTTON -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
            default -> log.warn("Unable to identify callback data {}, " +
                    "from chat {} in TEACHER_CLASS_MATERIALS handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_CLASS_MATERIALS;
    }
}
