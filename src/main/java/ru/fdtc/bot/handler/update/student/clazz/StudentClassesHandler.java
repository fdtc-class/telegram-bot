package ru.fdtc.bot.handler.update.student.clazz;

import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.constant.CallbackConstants;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.stream.Collectors;

@Component
public class StudentClassesHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public StudentClassesHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                 UserConnectionInfoService connectionInfoService,
                                 ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var classes = classInfoService.findActiveClasses(chatId);
        var buttons = classes.stream()
                .map(c -> Button.of(c.getSubjectName() + " " + c.getClassName(),
                        CallbackConstants.CLASS_ID, c.getId().toString()))
                .collect(Collectors.toList());
        buttons.add(Button.of(PreparedButton.TURN_BACK_BUTTON));

        var sendMessage = new SendMessage();
        if(classes.isEmpty()) {
            sendMessage.setText("Актуальных классов нет:(");
        } else {
            sendMessage.setText("Актуальные классы");
        }

        var keyboardMarkup = ButtonUtil.prepareColumn(buttons.toArray(new Button[]{}));
        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        clearMessage(update);
        var chatId = UpdateUtil.getChatId(update);
        var data = UpdateUtil.getButtonData(update);
        var connectionInfo = getUserConnectionInfoService().findById(chatId);
        var button = Button.of(data);
        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            switch (connectionInfo.getRole()) {
                case STUDENT -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_MAIN_PAGE);
                case TEACHER, ADMIN -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_MAIN_PAGE);
            }
        } else {
            switch (connectionInfo.getRole()) {
                case STUDENT -> getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASS_PAGE);
                case TEACHER, ADMIN -> getUserConnectionInfoService().setState(chatId, BotState.TEACHER_CLASS_PAGE);
            }
            classInfoService.save(chatId, button.payload().get(CallbackConstants.CLASS_ID));
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.STUDENT_CLASSES;
    }
}
