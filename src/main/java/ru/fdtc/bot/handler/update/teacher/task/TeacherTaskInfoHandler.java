package ru.fdtc.bot.handler.update.teacher.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

@Slf4j
@Component
public class TeacherTaskInfoHandler extends DefaultUpdateHandler {

    private static final String TASK_INFO_TEXT = """
            %s
            Описание: %s
            Минимальное кол-во баллов: %s
            Максимальное кол-во баллов: %s
            """;

    private final TaskInfoService taskInfoService;

    public TeacherTaskInfoHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                  UserConnectionInfoService connectionInfoService,
                                  TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var taskCache = taskInfoService.getCurrent(chatId);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareRaw(Button.of(PreparedButton.TURN_BACK_BUTTON));

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(TASK_INFO_TEXT.formatted(taskCache.getName(), taskCache.getDescription(),
                taskCache.getMinimumPoints(), taskCache.getMaximumPints()));
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            getUserConnectionInfoService().setState(chatId, BotState.TEACHER_TASK_PAGE);
        } else {
            log.warn("Unable to identify callback data {}, " +
                    "from chat {} in TEACHER_TASK_INFO handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.TEACHER_TASK_INFO;
    }
}
