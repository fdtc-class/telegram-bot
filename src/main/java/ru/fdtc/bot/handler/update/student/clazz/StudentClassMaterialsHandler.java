package ru.fdtc.bot.handler.update.student.clazz;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.ClassInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.List;

@Slf4j
@Component
public class StudentClassMaterialsHandler extends DefaultUpdateHandler {

    private final ClassInfoService classInfoService;

    public StudentClassMaterialsHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                        UserConnectionInfoService connectionInfoService,
                                        ClassInfoService classInfoService) {
        super(sender, publisher, connectionInfoService);
        this.classInfoService = classInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var files = classInfoService.getClassMaterials(chatId);

        files.forEach(file -> {
            var sendFile = new SendDocument();
            sendFile.setChatId(chatId);
            sendFile.setDocument(file);
            try {
                rememberMessage(getSender().execute(sendFile));
            } catch (TelegramApiException e) {
                log.error("Unable to send file {}", file, e);
            }
        });

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepare(List.of(Button.of(PreparedButton.TURN_BACK_BUTTON)));

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText("Материалы класса");
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws TelegramApiException {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            getUserConnectionInfoService().setState(chatId, BotState.STUDENT_CLASS_PAGE);
        } else {
            log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CLASS_INFO handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.STUDENT_CLASS_MATERIALS;
    }
}
