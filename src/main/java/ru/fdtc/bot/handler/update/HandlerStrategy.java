package ru.fdtc.bot.handler.update;

import lombok.RequiredArgsConstructor;
import ru.fdtc.bot.common.enumeration.BotState;

import java.util.Map;
import java.util.Optional;

@RequiredArgsConstructor
public class HandlerStrategy {

    private final Map<BotState, UpdateHandler> handlerMap;

    public Optional<UpdateHandler> getHandler(BotState state) {
        return Optional.ofNullable(handlerMap.get(state));
    }
}
