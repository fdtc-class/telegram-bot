package ru.fdtc.bot.handler.update.student.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.dto.Button;
import ru.fdtc.bot.common.dto.PreparedButton;
import ru.fdtc.bot.common.enumeration.BotState;
import ru.fdtc.bot.common.enumeration.EventSource;
import ru.fdtc.bot.common.util.ButtonUtil;
import ru.fdtc.bot.common.util.UpdateUtil;
import ru.fdtc.bot.event.UpdateEventPublisher;
import ru.fdtc.bot.handler.update.DefaultUpdateHandler;
import ru.fdtc.bot.service.TaskInfoService;
import ru.fdtc.bot.service.UserConnectionInfoService;

import java.util.Objects;

@Slf4j
@Component
public class StudentTaskStatusHandler extends DefaultUpdateHandler {

    private final TaskInfoService taskInfoService;

    public StudentTaskStatusHandler(DefaultAbsSender sender, UpdateEventPublisher publisher,
                                    UserConnectionInfoService connectionInfoService,
                                    TaskInfoService taskInfoService) {
        super(sender, publisher, connectionInfoService);
        this.taskInfoService = taskInfoService;
    }

    @Override
    public void doHandleBefore(Update update) throws Exception {
        var chatId = UpdateUtil.getChatId(update);
        var taskCache = taskInfoService.getCurrent(chatId);

        var sendMessage = new SendMessage();
        var keyboardMarkup = ButtonUtil.prepareRaw(Button.of(PreparedButton.TURN_BACK_BUTTON));


        var text = new StringBuilder();
        switch (taskCache.getPerformanceStatus()) {
            case WAITING_FOR_HANDING -> text.append("Вы ещё не отправили задание на проверку :(");
            case ON_CHECK -> text.append("Ваше решение находится на проверке");
            case CONFIRM -> {
                text.append("Задание выпонено!\nВы получили ")
                        .append(taskCache.getReceivedPoints())
                        .append(" балл(ов)");
                if(Objects.nonNull(taskCache.getTeacherFeedback())){
                    text.append("\nКомментарий преподавателя:\n").append(taskCache.getTeacherFeedback());
                }
            }
            case DECLINED -> text.append("Ваше решение было отозвано.\nКомментарий преподавателя:\n")
                    .append(taskCache.getTeacherFeedback());
        }

        sendMessage.setReplyMarkup(keyboardMarkup);
        sendMessage.setText(text.toString());
        sendMessage.setChatId(chatId);

        rememberMessage(getSender().execute(sendMessage));
    }

    @Override
    public void doHandleAfter(Update update) throws Exception {
        if(!update.hasCallbackQuery()) {
            return;
        }

        var chatId = UpdateUtil.getChatId(update);
        var callbackData = UpdateUtil.getButtonData(update);
        var button = Button.of(callbackData);
        clearMessage(update);

        if (PreparedButton.TURN_BACK_BUTTON.equals(button.prepared())) {
            getUserConnectionInfoService().setState(chatId, BotState.STUDENT_TASK_PAGE);
        } else {
            log.warn("Unable to identify callback data {}, " +
                    "from chat {} in CLASS_INFO handler", callbackData, chatId);
        }

        getPublisher().publish(update, EventSource.INTERNAL);
    }

    @Override
    public BotState botState() {
        return BotState.STUDENT_TASK_STATUS;
    }
}
