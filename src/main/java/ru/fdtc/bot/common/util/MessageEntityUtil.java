package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;
import org.telegram.telegrambots.meta.api.objects.EntityType;
import org.telegram.telegrambots.meta.api.objects.MessageEntity;

@UtilityClass
public class MessageEntityUtil {

    public MessageEntity bold(String text, String subtext) {
        return getMessageEntity(text, subtext, EntityType.BOLD);
    }

    public MessageEntity italic(String text, String subtext) {
        return getMessageEntity(text, subtext, EntityType.ITALIC);
    }

    private MessageEntity getMessageEntity(String text, String subtext, String type) {
        var offset = text.indexOf(subtext);
        var length = subtext.length();

        return MessageEntity.builder()
                .text(text)
                .length(length)
                .offset(offset)
                .type(type)
                .build();
    }
}
