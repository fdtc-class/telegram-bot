package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ClassUtil {

    public String getClassName(Object obj) {
        return obj.getClass().getSimpleName();
    }
}
