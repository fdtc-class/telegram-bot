package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;
import org.telegram.telegrambots.meta.api.methods.commands.SetMyCommands;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.commands.BotCommand;
import org.telegram.telegrambots.meta.api.objects.commands.scope.BotCommandScopeDefault;
import ru.fdtc.bot.handler.command.Command;

import java.util.Arrays;

@UtilityClass
public class CommandUtil {

    public boolean isCommand(Update update) {
        if(!update.hasMessage()) {
            return false;
        }

        return update.getMessage().isCommand();
    }

    public Command getCommand(Update update) {
        return Command.of(update.getMessage().getText());
    }

    public SetMyCommands getCommandsToSend() {
        var commandList = Arrays.stream(Command.values())
                .map(command -> new BotCommand(command.getName(), command.getDescription()))
                .toList();

        return new SetMyCommands(commandList, new BotCommandScopeDefault(), null);
    }
}
