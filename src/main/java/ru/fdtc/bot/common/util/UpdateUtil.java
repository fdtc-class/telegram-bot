package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.fdtc.bot.common.exception.ChatNotFoundException;

@UtilityClass
public class UpdateUtil {

    public Long getChatId(Update update) {
        if(update.hasMessage()) {
            return update.getMessage().getChatId();
        }

        if(update.hasCallbackQuery()) {
            return update.getCallbackQuery().getMessage().getChatId();
        }

        throw new ChatNotFoundException("Chat info is not found for update: %s".formatted(update));
    }

    public Integer getMessageId(Update update) {
        if(update.hasMessage()) {
            return update.getMessage().getMessageId();
        }

        if(update.hasCallbackQuery()) {
            return update.getCallbackQuery().getMessage().getMessageId();
        }

        throw Exceptions.messageNotFoundException("Message info is not found for update: %s\"".formatted(update));
    }

    public String getButtonData(Update update) {
        return update.hasCallbackQuery() ? update.getCallbackQuery().getData() : "UNDEF_UNDEF";
    }
}
