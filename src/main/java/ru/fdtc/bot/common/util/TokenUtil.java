package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TokenUtil {

    private final String BEARER = "Bearer ";

    public String prepare(String token) {
        return BEARER + token;
    }
}
