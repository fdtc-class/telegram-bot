package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;

import java.util.ArrayList;
import java.util.List;

@UtilityClass
public class IterableUtil {

    public <T> List<T> toList(Iterable<T> source) {
        var list = new ArrayList<T>();
        source.forEach(list::add);
        return list;
    }
}
