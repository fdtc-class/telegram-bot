package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import ru.fdtc.bot.common.dto.Button;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public class ButtonUtil {

    @SafeVarargs
    public InlineKeyboardMarkup prepare(List<Button>... buttonRaws) {
        var keyboard = Arrays.stream(buttonRaws)
                .map(buttonRaw -> buttonRaw.stream()
                        .map(Button::keyboardButton)
                        .toList())
                .toList();

        return new InlineKeyboardMarkup(keyboard);
    }


    public InlineKeyboardMarkup prepareKeyboard(List<List<Button>> buttonRaws) {
        var keyboard = buttonRaws.stream()
                .map(buttonRaw -> buttonRaw.stream()
                        .map(Button::keyboardButton)
                        .toList())
                .toList();

        return new InlineKeyboardMarkup(keyboard);
    }

    public InlineKeyboardMarkup prepareColumn(Button... buttons) {
        var column = Arrays.stream(buttons)
                .map(List::of)
                .toList();

        return prepareKeyboard(column);
    }

    public InlineKeyboardMarkup prepareRaw(Button... buttons) {
        var column = Arrays.stream(buttons)
                .toList();

        return prepareKeyboard(List.of(column));
    }
}
