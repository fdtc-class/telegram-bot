package ru.fdtc.bot.common.util;

import lombok.experimental.UtilityClass;
import ru.fdtc.bot.common.exception.ChatNotFoundException;
import ru.fdtc.bot.common.exception.MessageNotFoundException;

@UtilityClass
public class Exceptions {

    public ChatNotFoundException chatNotFoundException(String message) {
        return new ChatNotFoundException(message);
    }

    public MessageNotFoundException messageNotFoundException(String message) {
        return new MessageNotFoundException(message);
    }
}
