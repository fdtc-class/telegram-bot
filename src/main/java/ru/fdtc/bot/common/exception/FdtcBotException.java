package ru.fdtc.bot.common.exception;

public class FdtcBotException extends RuntimeException {

    public FdtcBotException(String message) {
        super(message);
    }
}
