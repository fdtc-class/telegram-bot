package ru.fdtc.bot.common.exception;

public class LoginException extends RuntimeException {

    public LoginException(String message) {
        super(message);
    }
}
