package ru.fdtc.bot.common.exception;

public class FileDownloadingException extends RuntimeException {

    public FileDownloadingException(String message) {
        super(message);
    }

    public FileDownloadingException(String message, Throwable cause) {
        super(message, cause);
    }
}
