package ru.fdtc.bot.common.dto;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Try;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SmartButton implements Button {

    private static final String REGEX = "_";
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String DEFAULT_BUTTON_ID = "custom";
    private static final String DEFAULT_BUTTON_TEXT = "UNDEFINED";

    private final String text;
    private final String buttonId;
    private final Map<String, String> payload;

    public SmartButton(PreparedButton prepared) {
        this.text = prepared.getText();
        this.buttonId = prepared.getId();
        this.payload = Collections.emptyMap();
    }

    public SmartButton(String text, Map<String, String> payload) {
        this.text = text;
        this.buttonId = DEFAULT_BUTTON_ID;
        this.payload = payload;
    }

    public SmartButton(PreparedButton prepared, Map<String, String> payload) {
          this.text = prepared.getText();
          this.buttonId = prepared.getId();
          this.payload = payload;
    }

    public SmartButton(String callbackData) {
        var rawData = split(callbackData);
        if(rawData.length != 2) {
            throw new IllegalArgumentException("Callback data has wrong format: " + callbackData);
        }

        this.buttonId = rawData[0];
        var prepared = getPrepared(buttonId);
        this.text = Objects.nonNull(prepared) ? prepared().getText() : DEFAULT_BUTTON_TEXT;
        this.payload = Try.of(() -> mapper.readValue(rawData[1].replace("COMMA", ","), new MapRef()))
                .getOrElse(new HashMap<>());
    }

    @Override
    public PreparedButton prepared() {
        return getPrepared(buttonId);
    }

    @Override
    public Map<String, String> payload() {
        return payload;
    }

    @Override
    public InlineKeyboardButton keyboardButton() {
        return InlineKeyboardButton.builder()
                .text(text)
                .callbackData(generateCallbackData(buttonId, payload))
                .build();
    }

    private static String generateCallbackData(String buttonId, Map<String, String> payload) {
        var serializedPayload = Try.of(() -> mapper.writeValueAsString(payload))
                .getOrElse("serialization-error");
        return buttonId + REGEX + serializedPayload.replace(",", "COMMA");
    }

    private static String[] split(String value) {
        return value.split(REGEX);
    }

    private static PreparedButton getPrepared(String buttonId) {
        return PreparedButton.of(buttonId);
    }

    private static class MapRef extends TypeReference<HashMap<String, String>> {}
}
