package ru.fdtc.bot.common.dto;

import lombok.Builder;
import lombok.Getter;
import ru.fdtc.bot.common.enumeration.UserRole;

import java.util.Set;

@Getter
@Builder
public class LoginResponse {

    private String token;
    private UserRole role;
}
