package ru.fdtc.bot.common.dto;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.Map;

/**
 * Button has the following callback data format:
 * "{buttonId}_{payload}"
 */
public interface Button {

    PreparedButton prepared();
    Map<String, String> payload();

    InlineKeyboardButton keyboardButton();

    /**
     * Factory methods to create Button instance
     */
    static Button of(String text, String payloadKey, String payloadValue) {
        return new SmartButton(text, Map.of(payloadKey, payloadValue));
    }

    static Button of(String text, String payloadKey1, String payloadValue1,
                     String payloadKey2, String payloadValue2) {
        return new SmartButton(text, Map.of(payloadKey1, payloadValue1, payloadKey2, payloadValue2));
    }

    static Button of(PreparedButton prepared, String payloadKey, String payloadValue) {
        return new SmartButton(prepared, Map.of(payloadKey, payloadValue));
    }

    static Button of(PreparedButton prepared, String payloadKey1, String payloadValue1,
                     String payloadKey2, String payloadValue2) {
        return new SmartButton(prepared, Map.of(payloadKey1, payloadValue1, payloadKey2, payloadValue2));
    }

    static Button of(String callbackData) {
        return new SmartButton(callbackData);
    }

    static Button of(PreparedButton prepared) {
        return new SmartButton(prepared);
    }
}
