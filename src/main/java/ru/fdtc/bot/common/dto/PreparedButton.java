package ru.fdtc.bot.common.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import ru.fdtc.bot.common.constant.Emoji;

import java.util.stream.Stream;

@Getter
@RequiredArgsConstructor
public enum PreparedButton {

    APP_INFO_BUTTON("Информация о приложении " + Emoji.PUPA, "AppInfo"),
    ACTUAL_CLASSES_BUTTON("Актуальные классы " + Emoji.TRAP, "ActualClasses"),
    TURN_BACK_BUTTON("Вернуться назад " +Emoji.BACK, "TurnBack"),
    CLASS_INFO("Информация о классе " + Emoji.PUPA, "ClassInfo"),
    TASKS("Задания", "Tasks"),
    MATERIALS("Материалы " + Emoji.BOOKS, "Materials"),
    CLASS_STATUS("Статус", "ClassStatus"),
    TASK_INFO("Информация о задании " + Emoji.PUPA, "TaskInfo"),
    TASK_FILES("Приложенные файлы " + Emoji.ATTACHED, "TaskFiles"),
    TASK_STATUS("Статус", "TaskStatus"),
    TASK_SUBMITTION("Сдать задание " + Emoji.MESSAGE, "SubmitTask"),
    ADMIN("Администратор " + Emoji.ADMIN, "Admin"),
    TEACHER("Преподаватель " + Emoji.TEACHER, "Teacher"),
    ADD_TEACHER("Добавить преподавателя", "AddTeacher"),
    YES("Да " + Emoji.NICE, "Yes"),
    NO("Нет" + Emoji.HELL_NO, "No"),
    GENERATE_PASSWORD_AUTO("Сгенерировать автоматически", "GeneratePasswordAuto"),
    APPROVE_FORM("Всё верно -> создать " + Emoji.NICE, "ApproveForm"),
    REFILL_FORM("Заполнить заново " + Emoji.BACK, "RefillForm"),
    CREATE_CLASS("Создать класс", "CreateClass"),
    CHECK_TASKS("Проверить задания", "CheckTasks"),
    STUDENTS_PERFORMANCE("Успеваемость студентов", "StudentsPerformance"),
    GROUPS("Группы", "Groups"),
    ADD_MATERIAL("Добавить материалы", "AddMaterial"),
    ADD_TASK("Добавить задание", "AddTask"),
    ADD_GROUP("Добавить группу", "AddGroup"),
    ADD_SUBJECT("Добавить новый предмет", "AddSubject"),
    ADD_TASK_FILE("Добавить новый файл", "AddTaskFile"),
    ACTIVATE_TASK("Активировать " + Emoji.GREEN_CIRCLE, "ActivateTask"),
    DECLINE("Вернуть " + Emoji.THUMB_DOWN, "Decline"),
    ESTIMATE("Оценить " + Emoji.THUMB_UP, "Estimate"),
    TURN_BACK_TO_CLASS_PERFORMANCE("Вернуться к списку работ " + Emoji.EYES, "TurnBackToClassPerformance"),
    TURN_BACK_TO_CLASS("Вернуться в класс " + Emoji.EYES, "TurnBackToClass"),
    EXAM("Экзамен", "Exam"),
    TEST("Зачёт", "Test"),
    DIFF_TEST("Дифференцированный зачёт", "DiffTest"),
    GET_TO_CLASS("Перейти в класс " + Emoji.EYES, "GetToClass"),
    GET_TO_TASK("Перейти к заданию " + Emoji.EYES, "GetToTask");

    private final String text;
    private final String id;

    public static PreparedButton of(String id) {
        return Stream.of(values())
                .filter(button -> button.id.equalsIgnoreCase(id))
                .findFirst()
                .orElse(null);
    }
}
