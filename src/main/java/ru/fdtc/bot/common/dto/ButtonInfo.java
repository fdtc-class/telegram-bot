package ru.fdtc.bot.common.dto;

import lombok.Builder;

import java.util.HashMap;
import java.util.Map;

@Builder
public class ButtonInfo {

    private PreparedButton prepared;
    private String text;

    @Builder.Default
    private Map<String, String> payload = new HashMap<>();

    public boolean isPrepared() {
        return prepared != null;
    }

    public PreparedButton getPrepared() {
        return prepared;
    }

    public String getText() {
        return isPrepared() ? prepared.getText() : text;
    }

    public Map<String, String> getPayload() {
        return payload;
    }

    public String getInfo(String key) {
        return payload.get(key);
    }

    public void putInfo(String key, String value) {
        payload.put(key, value);
    }

    public void removeInfo(String key) {
        payload.remove(key);
    }
}
