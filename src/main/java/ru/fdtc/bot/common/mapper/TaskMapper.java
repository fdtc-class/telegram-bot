package ru.fdtc.bot.common.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.fdtc.bot.data.entity.TaskCache;
import ru.fdtc.bot.rest.dto.response.TaskInfo;

@Mapper(componentModel = "spring")
public interface TaskMapper {

    @Mapping(target = "performanceStatus", source = "taskPerformanceInfo.status")
    @Mapping(target = "submittedFileName", source = "taskPerformanceInfo.fileName")
    @Mapping(target = "teacherFeedback", source = "taskPerformanceInfo.cause")
    @Mapping(target = "receivedPoints", source = "taskPerformanceInfo.points")
    TaskCache map(TaskInfo taskInfo);
}
