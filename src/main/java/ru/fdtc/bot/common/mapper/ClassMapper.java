package ru.fdtc.bot.common.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import ru.fdtc.bot.data.entity.ClassCache;
import ru.fdtc.bot.rest.dto.response.ClassInfo;
import ru.fdtc.bot.rest.dto.sealed.Professor;

@Mapper(componentModel = "spring")
public interface ClassMapper {

    @Mapping(target = "teacherName", source = "professor", qualifiedByName = "toName")
    ClassCache map(ClassInfo classInfo);

    @Named("toName")
    static String toName(Professor professor) {
        return professor.getLastName() + " " + professor.getFirstName().charAt(0) + ".";
    }
}
