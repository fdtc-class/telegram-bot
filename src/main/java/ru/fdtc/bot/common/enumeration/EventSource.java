package ru.fdtc.bot.common.enumeration;

public enum EventSource {
    INTERNAL, EXTERNAL
}
