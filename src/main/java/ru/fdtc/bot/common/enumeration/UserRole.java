package ru.fdtc.bot.common.enumeration;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@RequiredArgsConstructor
public enum UserRole {

    STUDENT("STUDENT"), TEACHER("PROFESSOR"), ADMIN("ADMIN");

    private final String value;

    public static UserRole of(String value) {
        return Arrays.stream(values())
                .filter(role -> value.equals(role.value))
                .findFirst()
                .orElse(null);
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
