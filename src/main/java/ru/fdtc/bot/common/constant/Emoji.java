package ru.fdtc.bot.common.constant;

public class Emoji {

    public static final String THUMB_UP = "\uD83D\uDC4D\uD83C\uDFFB";
    public static final String THUMB_DOWN = "\uD83D\uDC4E\uD83C\uDFFB";
    public static final String GREEN_CIRCLE = "\uD83D\uDFE2";
    public static final String RED_CIRCLE = "\uD83D\uDD34";
    public static final String WHITE_CIRCLE = "⚪️";
    public static final String BOOKS = "\uD83D\uDCDA";
    public static final String NICE = "✅";
    public static final String ADMIN = "\uD83D\uDC68\uD83C\uDFFB\u200D\uD83D\uDCBB";
    public static final String TEACHER = "\uD83D\uDC68\uD83C\uDFFB\u200D\uD83C\uDFEB";
    public static final String HELL_NO = "❌";
    public static final String ATTACHED = "\uD83D\uDCCE";
    public static final String PUPA = "\uD83D\uDD0E";
    public static final String MESSAGE = "\uD83D\uDCE9";
    public static final String TRAP = "\uD83E\uDEA4";
    public static final String BACK = "\uD83D\uDD19";
    public static final String RE = "\uD83D\uDD04";
    public static final String EYES = "\uD83D\uDC40";
    public static final String CLOCK = "\uD83D\uDD50";
    public static final String BANNED = "\uD83D\uDE15";
}
