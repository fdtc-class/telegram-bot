package ru.fdtc.bot.common.constant;

public class CallbackConstants {

    public static final String CLASS_ID = "classId";
    public static final String TASK_ID = "taskId";
    public static final String GROUP_ID = "groupId";
    public static final String HANGED_TASK_ID = "hangedTaskId";
    public static final String SUBJECT_ID = "subjectId";
}
