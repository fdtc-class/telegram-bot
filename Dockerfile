# Build stage
FROM maven:3-openjdk-17 AS build

COPY . /home/app/

COPY pom.xml /home/app/

WORKDIR /home/app

RUN mvn -f pom.xml clean package -Dmaven.test.skip

# Production

FROM openjdk:21-ea-17-slim-buster

COPY --from=build /home/app/target/bot-0.0.1-SNAPSHOT.jar /usr/local/app/


CMD ["java", "-jar", "/usr/local/app/bot-0.0.1-SNAPSHOT.jar", "--spring.config.location=classpath:application-prod.yaml"]
